# Blog de Victor Héry

Bienvenue sur le dépôt du blog de victor héry.

Ce blog est propulsé par [pelican](http://blog.getpelican.com/), versionné par [gogit](http://gogs.io/) et géré par moi-même.
Ce blog est sous licence Creative Commons, et toute participation est la bienvenue.

Si vous voyez des fautes dans mes articles, si vous souhaitez ajouter des précisions, ou participer et repartager le contenu, n'hésitez pas à cloner le dépot et à me faire des pull requests :)

Merci !
