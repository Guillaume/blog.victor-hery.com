Title: La virtualisation open source qui roxx
subtitle: ; Proxmox
Date: 2012-09-04 20:00
Modified: 2012-09-04 20:00
Category: Système
Tags: debian, proxmox, linux, virtualisation
keywords: debian, proxmox, linux, virtualisation
Slug: virtualisation-opensource-qui-roxx-proxmox
Authors: Victor
Status: published

[TOC]

## Proxmox, kesaco ?

<p>Comme je l'ai dit dans un précédent <a href="http://blog.héry.com/article7/la-virtualisation-ou-l-un-des-seul-bienfait-du-cloud">article sur la virtualisation</a>, je trouve cette manière de fonctionner sur un serveur particulièrement intéressante.</p>

<p>Comme je l'ai également dit à la fin de cet article, j'utilise pour ma part un outil de virtualisation open source nommé <a href="http://www.proxmox.com/">proxmox</a>.</p>

<p><img alt="Logo Proxmox" src="/images/proxmox_logo.png" style="width: 195px; height: 28px" /></p>

<p>&nbsp;</p>

<p>Proxmox est un packaging disponibles pour de nombreuses versions de linux/unix, mais il existe une distribution basée sur debian et maintenue par Proxmox, que j'utilise pour sa simplicité et sa compatibilité. (Au moins, pas besoin de se demander si tel ou tel module noyau fonctionnera ou pas une fois installé le package proxmox...)</p>

<p>Donc, la distribution debian Proxmox vous permet, une fois installée, de virtualiser via 2 techniques, OpenVZ et KVM, et de gérer toutes vos machines simplement via une interface web, de les sauvegarder en masse de manière programmée, tout en ayant un panel d'outils en ligne de commande sympathiques pour faire tout et n'importe quoi.</p>

<p>Il est également possible de mettre en place un cluster entre plusieurs Proxmox, d'accéder aux VM via l'interface web avec une applet Java, etc etc.</p>

<p>&nbsp;</p>

<p>Je vais donc détailler dans cet articles les fonctionnalités de Proxmox, parce que j'ai bien envie d'en parler :)</p>

## Proxmox, réseau bridgé

<p>Avec proxmox, vous êtes automatiquement lié à différentes interfaces "bridgées". Ou plutôt, des <a href="http://fr.wikipedia.org/wiki/Pont_(informatique)">interfaces de pont</a> en français. En gros, un switch virtuel dans le serveur.</p>

<p>C'est assez classique des hyperviseur, mais ça vaut le coup de le rappeller. Ca permet de lier les interfaces réseau des VM aux interfaces réseau physiques de l'hyperviseur, pour par exemple leur donner accès à Internet.</p>

<p>Ca permet également de créer des interfaces bridgés pour créer un réseau interne dans le serveur, permettant de joindre en interne toutes les VM. Totalement étanche à l'extérieur, ça ouvre des possibilités intéressantes en matière de service sécurisés&nbsp;:-)</p>

<p>L'utilisation de ce service permet vraiment de configurer tout et n'importe quoi de manière très simple. En effet vous pouvez ajouter de nombreuses interfaces à un bridge, par exemple des tap/tun pour openVPN, des interfaces dummy ou même d'autre bridges !</p>

<p>De plus, vous pouvez grâce à iptables créer des règles spécifique sur cette interface, pour la rendre totalement étanche, comme un vrai switch physique.</p>

<p>C'est sur ce principe qu'est basé le système réseau de proxmox, et il est vraiment très simple à utiliser et à exploiter.</p>

<p>&nbsp;</p>

## Containeurs OpenVZ, ou chroot en force

<p><a href="http://openvz.org"><img alt="Logo OpenVZ" src="/images/800px-openvz-logo.png" style="width: 200px; height: 50px" /></a></p>

### Quoique c'est ?

<p>Bon, soit vous êtes admin système et vous avez compris le titre, soit non. Dans mon métier, on aime les trucs binaires !</p>

<p>La virtualisation Open VZ va en fait consister à se servir du noyau système de l'hyperviseur pour faire fonctionner les machines virtuelles.</p>

<p>Cela a plusieurs effets, positifs comme négatifs.</p>

<p>En fait, vos machines virtuelles vont tourner en utilisant les ressources de base de l'hyperviseur. Contrairement à d'autres type de virtualisation, un OpenVZ va ainsi partager ses ressources directement avec l'hyperviseur.</p>

<p>Concrètement, si cela permet d'utiliser différentes distributions linux dans vos machines virtuelles, vous ne pourrez pas utiliser de Windows. Toutes vos distributions seront basées sur le noyau de l'hyperviseur, c'est à dire un noyau linux.</p>

<p>Installer un système Open VZ revient à choisir un "template", sorte d'image d'un véritable système d'exploitation, qui va être utilisé pour simuler ce système à destination de la machine virtuelle. De cette façon, du point de vue de la machine, le système sera correct et normal.</p>

<p>Ensuite, en se basant sur ce template, l'hyperviseur va réaliser un gros "<a href="http://fr.wikipedia.org/wiki/Chroot">Chroot</a>". Pour les néophytes ayant la flemme de lire wikipédia, l'hyperviseur va créer un dossier ayant une arborescence linux classique, puis va lancer la VM comme un processus classique en lui disant que son dossier racine / va être l'arborescence précedemment créée.</p>

<p>En conséquence, tout les processus lancées dans la VM seront vus depuis l'hyperviseur, alors que la VM sera une machine à part entière.</p>

### osef non ?

<p>Ben non, ça amène pas mal d'avantages intéressants.</p>

<ul>
	<li>La VM a un nouveau nom. On appelle ça un containeur (CT), et c'est plus classe</li>
	<li>Le CT n'a pas vriament besoin de démarrer ou redémarrer, vu que c'est un processus. Donc, quand on démarre ou redémarre un containeur, ça dure approximativement 10 secondes. (Sans SSD !!)</li>
	<li>Le CT partage pas mal de configuration noyau avec l'hyperviseur. La gestion du swap ou de la RAM par exemple</li>
	<li>Le CT n'a pas besoin d'un certain nombre de service (jamais de client NTP dans un CT !)</li>
	<li>Le CT consomme extrêmement peu de ressource, même avec de l'apache, du mysql et autre (à charge raisonnable bien sûr...)</li>
	<li>Le CT peut utiliser du réseau bridgé, ou entièrement virtualisé (plus d'adresse MAC nécessaire)</li>
	<li>On peut facilement imaginer plusieurs centaines de CT sur un serveur sans trop de problème.</li>
	<li>Comme l'intérieur des CT sont des dossier de l'hyperviseur, on peut modifier, sauvegarder et récupérer très simplement le contenu des CT.</li>
	<li>On peut à n'importe quel moment augmenter en temps réel la taille du disque dur allouée au CT, même si celui est en fonctionnement.</li>
</ul>

<p>Il y a surement plein d'autres choses, mais globalement c'est déjà des points très intéressants, selon les usages qu'on en a.</p>

<p>Par contre, comme d'habitude, il y a aussi quelques inconvénients. Le noyaux étant émulé, il y a des choses qui ne fonctionnent pas dans le CT.</p>

<ul>
	<li>N'essayez jamais d'installer un client NTP. Le CT fonctionne avec celui de l'hyperviseur, et il peut y avoir méchant conflit</li>
	<li>Au revoir certains modules noyaux particulier, notamment IPSec...</li>
	<li>Les machines virtuelles java n'aiment pas trop OpenVZ (sauf l'obscure machine d'IBM) Donc, pas de java sur OpenVZ (&lt;troll&gt;Des failles de sécurité en moins ! &lt;/troll&gt;)</li>
	<li>Il faut activer des choses depuis l'hyperviseur pour utiliser OpenVPN dans un CT (mais une fois fait, ça marche nickel)</li>
	<li>La gestion du swap, de la RAM sont basés sur la configuration de l'hyperviseur. Enfin, ce n'est pas forcément un inconvénient</li>
	<li>Il y a souvent des manip' un peu particulière à faire pour utiliser des services touchant au matériel ou au noyau, mais là encore on s'en tire</li>
</ul>

<p>&nbsp;</p>

<p>Bref, il y a du pour et du contre. Perso, sauf pour certains usages très précis (IPSec ou tomcat avec Java...), je n'utilise que des CT sous OpenVZ. Optimisées et ultra rapides, ça serait vraiment dommage de s'en passer.</p>

<p>&nbsp;</p>

## KVM, le mode de secours

<p><img alt="Logo KVM" src="/images/kvmbanner-logo2.png" style="width: 200px; height: 62px" /></p>

<p>Bon, si vous tenez vraiment à faire tourner tomcat, à utiliser IPSec, voir à installer un client NTP. Ou pire, que vous n'aimez pas OpenVZ ou que vous tenez à utiliser Windows (ouh ! Haro !), proxmox n'a pas mis tous les oeufs dans le même panier et propose un deuxième système de virtualisation open source, j'ai nommé <a href="http://www.linux-kvm.org/page/Main_Page">KVM</a>.</p>

<p>Une machine KVM est cette fois une véritable machine virtuelle, complètement indépendante de l'hyperviseur. Elle possède son propre noyau, ses propres interfaces réseaux et sa carte graphique, un disque dur bien défini, etc.</p>

<p>Vous pouvez donc tout à fait l'utiliser pour faire tourner un système Windows si vous le souhaitez.</p>

<p>Cette technique de virtualisation est robuste, mais aucun des avantages d'OpenVZ ne peut être appliqué à elle. En contrepartie, la plupart des inconvénients d'OpenVZ n'a pas de prise sur une KVM&nbsp;:-)</p>

<p>Etant une "vraie" machine, une KVM va pouvoir utiliser ses propres modules noyaux (bonjour, IPSec), ainsi qu'une machine virtuelle java. On l'installe depuis une image iso classique, comme n'importe quel système d'exploitation.</p>

<p>Pour ma part, je ne m'éteindrai pas trop dessus, car je n'utilise les KVM que si mon besoin ne peut vraiment pas se poser sur OpenVZ.</p>

<p>KVM est certes plus complet, mais par rapport à OpenVZ, le fait d'avoir une vraie machine à émuler est aussi beaucoup plus lourd. Ca demande plus de ressource à l'hyperviseur, c'est bien plus long à éteindre et démarrer qu'un containeur, on est obligé d'y accéder via SSH ou avec les outils proxmox, ...</p>

<p>Bref&nbsp;:-)</p>

<p>&nbsp;</p>

## Interface web, le bonus qui en jette

<p>Bon, je ne vous ferait pas plein de screens super bô de l'interface web de gestion proxmox.</p>

<p>Accessible nativement en https (c'est le bien), elle permet de quasiment tout gérer pour vos tâches quotidienne.</p>

<ul>
	<li>Création de machines virtuelles en 3 clics (OpenVZ et KVM)</li>
	<li>Modifications des ressources allouées à ces machines (RAM, disques, nombres d'interfaces réseau, ...)</li>
	<li>Gestion des stockages (disques dur machines, images iso, templates OpenVZ, ...)</li>
	<li>Management des machines (éteindre, démarrer, couper à chaud ou à froid, accès à une console VNC ou un shell)</li>
	<li>Création de cluster entre différents serveurs proxmox</li>
	<li>Mise en place de <a href="http://fr.wikipedia.org/wiki/Haute_disponibilité">Haute Disponibilité</a> (un peu de ligne de commande nécessaire selon les configurations)</li>
	<li>Gestion de TOUTES les machins d'un cluster depuis UNE seule interface web (trop le pied !)</li>
	<li>Gestion d'utilisateurs, de groupes d'utilisateurs, de droits d'accès à des machines (qui a dit "Vendons du <a href="http://blogvps.kicou.org/">VPS</a> !")</li>
	<li>Gestion de l'hyperviseur lui-même (redémarrage, accès shell, monitoring du traffic réseau, de la charge processeur, de la RAM, du SWAP, du...)</li>
	<li>Gestion des sauvegardes, journalières, mensuelles, incrémentales, ...</li>
	<li>Gestion du stockage réseau, du stockage local, de l'iscsi</li>
	<li>...</li>
</ul>

<p>GLobalement, avec simplement l'interface web, vous faites tourner tout un tas de machines virtuelles seulement en cliquant 2 ou 3 fois, depuis une interface web sécurisée.</p>

<p>En rajoutant un peu de ssh et de ligne de commande, vous avez un service pro, redondé, sécurisé, permettant de lancer votre propre service de VPS dans votre salon si vous avez envie ! Après bien sûr, à vous de négocier votre bande passante avec votre FAI (ainsi qu'avec ses <a href="http://wiki.auto-hebergement.fr/fournisseurs/fai">Conditions Générales de Vente</a>)</p>

<p>&nbsp;</p>

<p>A plus petite échelle (la mienne par exemple) vous pouvez simplement sauvegarder et redémarrer votre serveur web que vous avez pris 3 jours de temps libre à configurer aux petits oignons. (Enfin, surtout le serveur courriel en fait...), et ce même si votre serveur maison claque brusquement.</p>

<p>&nbsp;</p>

## Moi, je suis convaincu (sinon j'en ferai pas la pub)

<p>Auneffet, ce en quoi je crois, j'en parle. Simple et efficace&nbsp;:-)</p>

<p>Maintenant, ce long article servant d'introduction à d'autres prévus sur la configuration de proxmox, j'espère que vous l'aurez apprécié.</p>

<p>Désolé, c'est long. Mais c'est bon aussi&nbsp;;-)</p>

<p>&nbsp;</p>
