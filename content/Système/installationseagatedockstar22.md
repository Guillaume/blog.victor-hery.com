Title: Installation d'un seagate dockstar 2/2
subtitle: ; Parlons dockstar
Date: 2012-08-13 21:54
Modified: 2012-08-13 21:54
Category: Système
Tags: système, seagate dockstar, debian
keywords: système, seagate dockstar, debian
Slug: installation-dockstar-22
Authors: Victor
Status: published

[TOC]

# Installation de debian sur le dockstar

Dans la première partie, nous avons déballé et branché le dockstar.

Nous avons également vu comment nous connecter au dockstar, bloquer ses mises à jour constructeur, etc... Pour lire la première partie, rendez vous [ici]({filename}installationseagatedockstar12.md)

Maintenant, passons aux choses sérieuses : l'installation de debian sur notre dockstar ! Et pour commencer voyons un peu à quoi nous pouvons nous attendre...

## Découvrons-en un peu plus

Quelques commandes de base de tout système linux vont nous permettre de découvrir un peu les capacités de ce dockstar.  

Par exemple,
```
 uname -a
```

nous donne :
```
 Linux Pogoplug 2.6.22.18 #57 Mon Aug 31 16:31:01 PDT 2009 armv5tejl unknown
```

Notre dockstar tourne donc avec un linux pogoplug, plutôt minimaliste. Il est cependant possible d'en faire quelque chose :)  
Nous pouvons également voir que le dockstar est monté sur une architecture ARM (comment ça on le savait déjà ?). Il nous faudra y penser lorsque nous installerons des logiciels. En effet, les architectures ARM sont inhabituelles, et certains paquets debian ne seront pas compatibles. Une recompilation à partir des sources pourra s'avérer nécessaire, mais nous en reparlerons.  

Si nous effectuons dans la foulée un
```
cat /proc/cpuinfo
```

nous obtenons toutes les informations sur le processeur de la bête. Architecture ARM, puissance, ...  

Pour avoir des informations sur la mémoire RAM :  
```
free -m
```

Bien. Attaquons nous à Débian lui-même maintenant !  

## Installation de debian

### Partionnement

Pour installer debian, il vous faut une clé USB. N'importe laquelle fonctionnera, mais je vous conseille de prendre une clé d'au moins 2Go, afin d'avoir assez d'espace pour installer diverses choses, tout en ayant de la place pour une mémoire swap. (si vous ne savez pas ce qu'est une mémoire swap, rendez-vous ici)  

Sachez également que plus votre clé USB est de qualité et rapide, plus le système sera réactif.  


Pour permettre l'utilisation du système, il va vous falloir brancher votre clé sur le port usb à gauche quand vous regardez votre dockstar de derrière. Sinon, il risque d'y avoir des conflits avec le périphérique USB qui sera éventuellement branché sur cette prise. En effet, ce port est considéré comme sda par le système pogoplug, et donc prioritaire.  
Branchez donc votre clé sur ce port.  

Vérifiez grâce à
```
/sbin/fdisk -l
```
si votre clé est bien détectée sur sda. Nous allons maintenant la formater proprement avant installation.  

Lancez
```
/sbin/fdisk /dev/sda
```

Vous êtes maintenant dans l'interface de configuration de ce magnifique utilisaire qu'est fdisk. Très puissant, mais il faut l'avouer un peu frustre. Qu'importe, allons-y :)  

Tapez d pour effacer les éventuelles partitions déjà existantes. Validez par Enter.  

Ensuite, tapez n pour créer une nouvelle partition. Choisissez primary, puis le numéro 1 pour cette partition. Décidez de votre taille, variable selon la capacité de votre clé. N'oubliez pas de garder un peu d'espace pour le swap. Je vous conseille un swap de 512Mo, pour compenser le peu de RAM que possède le dockstar.  
Une fois cette partition créée, tapez de nouveau n, puis créez la deuxième partition pour le swap (primary, partition n°2). Bien sûr, vous pouvez créer autant de partitions que vous voulez, mais une partition primaire et une autre pour le swap sont suffisantes.  

Une fois votre partionnage terminé, tapez t, puis entrez le numéro de partition swap (2 dans notre exemple). Entrez ensuite 82 pour définir cette partition comme swap linux.  
Il ne reste plus qu'à taper w pour écrire les partitions et enregistrer les modifications.  

### Téléchargement

Maintenant que notre clé est prête, nous allons commence le téléchargement et l'installation de Debian.  

Tapez
```
cd /tmp
```
pour aller dans la partie temporaire du système. Tout ce qui est placé dans ce dossier est supprimé à chaque reboot. C'est fort pratique pour télécharger des fichiers temporaires, comme des sources par exemple, qui seront supprimées une fois le logiciel installé.  

On télécharge debian squeeze :
```
wget http://jeff.doozan.com/debian/dockstar.debian-squeeze.sh
chmod +x dockstar.debian-squeeze.sh
export PATH=$PATH:/usr/sbin:/sbin
./dockstar.debian-squeeze.sh
```

Répondez "Ok" à la première question. Surtout, ne débranchez pas le dockstar pendant le téléchargement, au risque de le rendre hors d'état.  
Le script d'installation va installer uBoot, pour permettre de booter sur une clé USB, ce que par défaut le dockstar ne permet pas.  

Les mises à jour automatiques du pogoplug sont désactivées, puis l'installation commence. Il y en a pour 15-20 minutes selon votre connexion. Chaque package est téléchargé et validé, vous pouvez regarder les différents modules installés pour en apprendre plus sur une distribution linux si vous le souhaitez :-)  

Enfin, le script vous informe que le mot de passe root par défaut sera root. Assez peu sécurisé, nous le changerons dès la connexion. Tapez Y quand le script vous demande de redémarrer.  

Il n'y a plus qu'à attendre que le dockstar ne redémarre. Il met environ une 30aine de secondes, mais vous pouvez décider de le pinguer jsuqu'à ce qu'il réponde pour gagner du temps. Il reprendra la même IP qu'au début, sauf si vous avez un serveur DHCP, auquel cas il risque de lui donner une autre adresse. Utilisez netdiscover ou nmap pour le découvrir si vous n'arrivez pas à vous connecter.  

### Configuration de Debian

Connectez vous en ssh sur le dockstar avec le login ``root``, mot de passe ``root``.

Utilisez tout de suite
```
passwd
```
pour changer le mot de passe root. Choisissez le mot de passe qui vous convient.  

L'installation de tous les services que vous désirez se passe maintenant comme dans un système debian classique, avec apt-get et apt-cache.  

Pour ce qui est de la configuration en elle-même du dockstar, il est probable que vous souhaitiez installer un serveur FTP pour accéder à vos disque dur externe, ainsi que le système de partage de fichier windows Samba.  

Pour cela :
```
apt-get install pure-ftpd samba
```
J'utilise pure-ftp comme serveur ftp car il permet une gestion simple des utilisateurs, mais vous pouvez bien sûr utiliser le serveur qui vous sied le mieux.  

Je vous conseille également à titre personnel d'installer aptitude pour mieux gérer les paquets, nano pour faire de l'édition de texte plus simple qu'avec vi, ntp pour maintenir le système à l'heure, et screen pour utiliser plusieurs terminals sur le même écran.  
```
apt-get install aptitude screen nano ntp
```

## Et voila !

Et voila, votre dockstar est completement fonctionnel sous debian ! Si jamais votre clé USB plante, il vous suffira de réinstaller tout ça sur une nouvelle clé. Vous pouvez également faire un backup de votre clé sur votre PC personnel, surtout si vous avez mis en place une configuration pointue.  

Pour ma part, j'utilise mon dockstar 24/24 comme serveur FTP, serveur de backup avec rsync, partage de fichier samba et serveur de version type Dropbox grâce à [sparkleshare](sparkleshare.org/). Il faut avouer qu'il marche parfaitement, et ce depuis plus de 7 mois maintenant :)  

Attention par contre à ne pas trop lui en demander. Il peut héberger un serveur web, mais préférez un serveur léger type [Nginx](nginx.net/) à apache.  
Sachez également qu'un serveur de mail postfix/dovecot peut tourner, mais après tests, installer les applications qui s'imposent à côté (type spamassassin) a eu pour résultat de saturer la mémoire RAM et de planter le dockstar. Il peut être intéressant de se servir du dockstar comme test pour vous amuser si vous voulez :)  
Puis de vous construire un petit serveur personnel ensuite ! S'auto-héberger reste une solution viable et intéressante pour s'amuser ! Référez vous aux articles sur Dovecot, postfix et autre de ce site si vous avez apprécié cet article.  

## Sources

[Installation et déballage d'un Seagate Dockstar](blog.crifo.org/post/2010/10/02/Seagate-DockStar-deballage-et-installation-de-debian)

[Installation et utilisation de pure ftp](www.debianaddict.org/article71.html)

[Nginx](nginx.net/)

[Installation de samba sur Linux](blog.crifo.org/post/2010/10/09/Partager-ses-donnees-avec-samba-depuis-Linux)

[Si vous oubliez votre mdp root mysql](www.commentcamarche.net/faq/9773-my-sql-reinitialiser-le-mot-de-passe-root) ;)
