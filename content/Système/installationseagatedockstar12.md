Title: Installation d'un seagate dockstar 1/2
subtitle: ; Parlons dockstar
Date: 2012-08-12 21:54
Modified: 2012-08-12 21:54
Category: Système
Tags: système, seagate dockstar, debian
keywords: système, seagate dockstar, debian
Slug: installation-dockstar-12
Authors: Victor
Status: published

[TOC]

# Déballage et configuration initiale

Bienvenue sur cet article, dont l'objectif du jour va être de vous expliquer au mieux et simplement, comment installer, mettre en place et utiliser un système d'exploitation Debian sur la plate-forme matérielle Seagate Dockstar FreeAgent.

## Présentation :

Le Seagate Dockstar FreeAgent est un système vendu par Seagate à l'origine pour permettre à ses clients d'accéder à leur disques durs (branchés sur le Dockstar) depuis leur interface web, accessible via un abonnement mensuel, plutôt cher. Dès le début, ça n'a pas marché, les prix ont chutés, Seagate à diminué drastiquement les coûts de l'abonnement mensuel et de la machine en elle-même, sans résultats probant sur les ventes. Jusqu'au jour où...

Jusqu'au jour où un petit malin s'est rendu compte que cette sympathique machine disposait d'un accès root ssh débloqué, et qu'à ce titre il était tout à fait possible d'installer un système d'exploitation Debian Squeeze, débloquant ainsi complètement les possibilités de la machine pour en faire un serveur personnel à moindre coût, performant et surtout consommant une quantité ridicule de courant (8Watts à pleine charge).

Dès lors, il est devenu assez difficile de s'en procurer, et les prix ont grimpés, même si Seagate a désespérément tenté différentes mises à jour pour bloquer cet accès ssh et ainsi enrayer le détournement de son matériel.

Le but de cet article est de vous montrer comment utiliser le Dockstar pour justement éviter ces mises à jour, installer un système d'exploitation Debian pleinement fonctionnel, puis certains services intéressants même pour un usage personnel. (la puissance de la machine restant relativement limité)  
Pour plus d’information sur le Dockstar lui-même, voyez [ici)(www.seagate.com/www/fr-fr/products/network_storage/freeagent_dockstar/#tTabContentOverview)

Je tiens à signaler que le blog de Dju m'a été d'une grande aide pour écrire cet article :

[Installation et déballage d'un Seagate Dockstar](blog.crifo.org/post/2010/10/02/Seagate-DockStar-deballage-et-installation-de-debian)

Vous trouverez un détail des sources m'ayant servit pour cet article à la fin de l'article.

## Démarrage et découverte

Tout d'abord, il va vous falloir déballer puis brancher votre Dockstar.  
**ATTENTION!** *Pour brancher le cable réseau, faites impérativement attention à ce qu'il ne soit pas relié à Internet ! Auquel cas, le Dockstar téléchargerait automatiquement ses dernières mises à jour, désactivement irrémédiablement l'accès ssh, rendant ainsi votre Dockstar inutile pour toute la suite de cet article. Utilisez donc un switch ou un routeur non relié à Internet, ou branchez directement votre Dockstar à votre ordinateur (celui-ci n'étant bien sûr pas configuré pour servir de passerelle Internet)*

Une fois ce branchement effectué, nous allons pouvoir attaquer le vif du sujet. La diode à l'avant du Dockstar doit clignoter en orange, vous indiquant qu'il ne parviens pas à télécharger ses mises à jour, ce qui nous convient parfaitement. Pas lieu de s'inquiéter de ce frénétique clignotement orange donc :)

Ici, deux cas :  
 - Vous avez branché votre Dockstar sur un routeur utilisant le DHCP, auquel cas le Dockstar et votre ordinateur disposent d'une adresse IP automatiquement attribuée, il vous suffit de trouver laquelle
possède le Dockstar. Vraisemblablement, utiliser l'interface du routeur est une bonne idée, si vous êtes dans ce cas vous êtes sans doute à même de récupérer cette information d'après le modèle de routeur que vous utilisez.  
 - Vous avez branché votre Dockstar sur un switch ou sur votre ordinateur, le Dockstar va donc utiliser son adresse IP par défaut, qu'il va vous falloir découvrir.

Deuxièmement, deux nouveaux cas :  
 - Vous êtes sous un système Unix (Linux ou Mac OS). Dans ce cas, peu de problème, vous disposez d'un terminal à même de remplir tous les services possibles. Vous devez avoir les applications ifconfig, ssh et nmap d'installées par défaut. Pour plus de rapidité, vous pouvez installer netdiscover à la place de nmap (aptitude install netdiscover sur une Debian like)  
 - Vous êtes sous un système Windows. Pour une fois, on ne vous en voudra pas, tout le monde ne peut pas être parfait, au moins vous avez acheté un Dockstar et vous êtes curieux, parfait :)  
 - Vous allez devoir télécharger les outils. Récupérez donc nmap et putty. Nmap va vous permettre de trouver l'adresse du Dockstar, putty de vous y connecter en ssh.

Nous sommes prêts pour la suite.  

Le Dockstar prend par défaut une adresse IP du type 169.254.xxx.xxx. Pour la trouver, il va donc falloir que vous configuriez votre interface réseau en conséquence, puis que vous scanniez les différents matériels existant sur votre réseau.  

**Linux et Mac OS :**  

Rien de bien compliqué. Ouvrez un terminal, logguez vous en administrateur (ou utilisez sudo), puis reconfigurez votre interface réseau :  
```
ifconfig eth0 169.254.1.0/16
```

Scannez le réseau pour trouver l'adresse du Dockstar :  
```
netdiscover -r 169.254.0.0/16 -P
```

Ou avec nmap :  
```
nmap 169.254.0.0/16
```

Notez que nmap est beaucoup plus long que netdiscover. Vu qu'il va falloir scanner 254*254 adresses, mieux vaut utiliser un outil le plus rapide possible ;-)  

Avec netdiscover, vous obtenez quelque chose dans ce goût là :  
```
Currently scanning: 169.254.87.0/16 | Our Mac is: 00:1e:8c:xx:xx:xx - 0

	1 Captured ARP Req/Rep packets, from 1 hosts. Total size: 60

	_____________________________________________________________________________

	IP At MAC Address Count Len MAC Vendor

	-----------------------------------------------------------------------------

	169.254.214.19 00:10:75:xx:xx:xx 01 060 Maxtor Corporation
```

L'adresse de votre Dockstar est donc le 169.254.214.19 (adaptez ici avec ce que vous obtenez comme résultat bien sûr)  

Si vous utilisez ou utiliserez pour la connexion à Internet un serveur DHCP, vous pouvez noter l'adresse MAC du Dockstar pour le configurer en IP fixe dans votre serveur DHCP. Ainsi, il sera toujours facilement accessible plus tard.  

**Windows :**  

Sous Windows, une fois téléchargé nmap, il vous faudra le dézipper, puis vous rendre dans le dossier avec un terminal DOS. Utilisez cd dans votre terminal pour atteindre le dossier de nmap.  

Il va également falloir reconfigurer votre interface réseau avec l'adresse 169.254.1.0 et le netmask 255.255.0.0.  

Ensuite, retournez sur votre terminal, et entrez  
```
nmap -sP 169.254.1.0/16
```

Il vous faut ensuite attendre que nmap scanne le réseau et découvre l'adresse du Dockstar. Cela peut être un peu long (ok, très long), donc dès que vous voyez apparaître une ligne du type host 169.254.214.19 is UP, vous pouvez faire ctrl+C pour arrêter le scan : vous avez trouvé votre Dockstar. Notez bien son adresse IP.  

## Connexion :  

Voila, vous avez l'adresse IP du Dockstar, vous êtes donc prêts à vous connecter dessus et à mettre les mains dans le cambouis.  

Pour cela, connectez vous en ssh :  

**Sous Mac OS/Linux ;**  
```
ssh root@169.254.214.16
```
puis mot de passe ``stxadmin``

**Sous Windows**, lancez putty, entrez l'adresse Ip du Dockstar, puis logguez vous en root avec le mot de passe stxadmin.

Globalement, accès ssh :  
  - Identifiant : root  
  - Mot de passe : stxadmin  

Notez que le Dockstar a également un accès telnet de débloqué, mais nous préférerons utiliser ssh, plus sécurisé (Même si vous êtes en connexion directe avec le Dockstar pour l'instant, ce n'est pas une raison pour négliger cet aspect ;) Et ça donne de bonnes habitudes)  

Vous voilà donc connecté sur la bête !  

## Connecté !

Une fois connecté, la toute première chose à faire est de monter la partition du système en lecture/écriture, car elle est par défaut uniquement autorisée à la lecture. Pour se faire :  
```
mount / -rw -o remount
```

Deuxième chose importante, empêcher le système de se mettre à jour une fois qu'il se sera connecté à Internet, histoire d'éviter les désagrément du type désactivation de l'accès ssh/telnet. Pour cela, on va indiquer au Dockstar que les adresses sur lesquelles il se connecte pour se mettre à jour sont en fait lui-même (localhost). Ainsi, il va tourner en boucle en demandant à lui-même ses mises à jour, n'obtenant jamais de réponse. On est ainsi sûr qu'il ne se mettra pas à jour :  
```
	echo "127.0.0.1 service.pogoplug.com" > /etc/hosts
	echo "127.0.0.1 pm1.pogoplug.com" >> /etc/hosts
	echo "127.0.0.1 pm2.pogoplug.com" >> /etc/hosts
	echo "127.0.0.1 service.cloudengines.com" >> /etc/hosts
	echo "127.0.0.1 upgrade.pogoplug.com" >> /etc/hosts
```

Ceci est en fait une précaution nécessaire. Pour optimiser cette solution, nous allons carrément empêcher le service popoplug de se lancer au démarrage, ça fera de la ressource supplémentaire, et on en entendra plus parler :)  

Editez le fichier /etc/init.d/rcS avec un bon vieux :  
```
vi etc/init.d/rcS
```

(Si vous ne maitrisez pas vi, un petit aperçu des commandes principales pourrait vous être utile : [Débuter avec vi](fr.wikipedia.org/wiki/Vi#D.C3.A9buter_avec_vi))

Dans ce fichier, cherchez la ligne « /etc/init.d/hbmgr.sh start » et mettez là en commentaire :  
```
/etc/init.d/hbmgr.sh start
```

devient :  
```
 #/etc/init.d/hbmgr.sh start
```

Le # sert de commentaire dans ce fichier.  

Voila le préambule à la connexion effectué. Maintenant, vous pouvez connecter votre Dockstar en toute sécurité sur Internet, il ne pourra plus se mettre à jour, ni vous ennuyer.  

Cependant, avant d'aller plus loin, poussons la curiosité et découvrons un peu ce qu'il a dans le ventre.  

La deuxième partie est disponible [ici]({filename}installationseagatedockstar22.md)
