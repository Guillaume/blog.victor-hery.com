Title: Construction du cluster (openvpn)
subtitle: ; cluster Proxmox distant (2/4)
Date: 2013-02-02 13:35
Modified: 2013-02-02 13:35
Category: Système
Tags: proxmox, debian, iptables, linux, sécurité, tunnel, cluster, haute disponibilité
keywords: proxmox, debian, iptables, linux, sécurité, tunnel, cluster, haute disponibilité
Slug: cluster-proxmox-distant-2.4-construction-cluster
Authors: Victor
Status: published

[TOC]

## Commençons

<p>Bien, cet article est la partie 2 de ma série d'articles sur la mise en place d'un cluster Proxmox.</p>

<p>Dans cet article, je vais partir du début et parler de l'installation des 3 serveurs Proxmox dont nous allons avoir besoin.</p>

<p>Pour rappel :&nbsp;</p>

<ul>
	<li>Un serveur principal, chargé de faire tourner les principaux services sous forme de machines virtuelles</li>
	<li>Un serveur de secours, moins costaud mais suffisant pour faire tourner les services que vous considérez comme vitaux</li>
	<li>Un 3ème serveur, pas forcément très performant, qui va principalement servir à assurer le quorum, afin que 3 machines soient dans le cluster</li>
</ul>

<p>Vous trouverez tous les détails dans la partie 1 de cette série,&nbsp;<a href="/2013/01/cluster-proxmox-distant-1.4-concept.html">Cluster Proxmox distant (1/4) : le concept</a></p>

<p>&nbsp;</p>

<p>Le petit schéma pour rappeler l'infrastructure et les noms des machines :</p>

<p><img alt="" src="/images/schemaHA(1).png" style="width: 612px; height: 353px;" /></p>

&nbsp;

## Matériel, serveurs, spécificités et autres joyeusetés

<p>Comme dit plus haut, vous allez avoir besoin de 3 serveurs. Les 2 principaux, je conseille qu'ils soient physiques. De vrais serveurs avec les perfomances dont vous avez besoin.</p>

<p>Le 3eme serveur, servant uniquement pour le quorum, peut être une simple machine virtuelle dans un coin. L'important est que vous puissiez installer proxmox dessus. (Pour ma part, c'est une KVM avec proxmox à l'intérieur, qui tourne sur une autre architecture Proxmox chez moi :) )</p>

<p><strong>Par contre, ne montez pas votre Arbitre sur une KVM dans l'un des 2 premiers serveurs. Sinon vous perdez tout l'intérêt d'avoir un 3eme système indépendant des 2 autres pour assurer le quorum !</strong></p>

<p>&nbsp;</p>

<p>Vous allez également avoir besoin d'installer Proxmox sur vos serveurs. Dans ces articles, je me baserai sur l'installation de <a href="http://www.proxmox.com/downloads/proxmox-ve">Proxmox 2.X standard</a>, fournie par Proxmox et donc basée sur Debian Squeeze (actuellement Proxmox 2.2).</p>

<p>L'installation de base présente l'avantage de proposer un partionnement des disques dur via <a href="http://fr.wikipedia.org/wiki/LVM">LVM</a>, qui est très pratique pour redimensionner les partitions et utiliser DRBD par la suite. Cependant, je détaillerai le partionnement nécessaire, afin que vous puissiez vous en tirer si vous souhaitez installer Proxmox par dessus une autre distribution Linux.</p>

<p>OVH et Online proposent la distribution Proxmox à l'installation. OVH vous permet de choisir le partionnement via leur interface web, ce qui est assez pratique. Online par contre part sur un partionnement sans LVM qui ne vous permettra pas d'utiliser simplement DRBD par la suite.&nbsp;<strong>Attention donc chez Online, mieux vaut passer par leurs cartes ILO/iDrac pour installer Proxmox depuis l'ISO.</strong></p>

<p>&nbsp;</p>

<p>Enfin, vous allez avoir besoin d'un PC supportant le SSH pour vous connecter aux différents serveurs. Je vous conseillerai bien un Linux, parce que vous allez passer pas mal de temps connecté, donc un système supprotant bien le bash (et notamment l'encodage de caractère et les couleurs) est préférable. Cependant, Windows avec putty fonctionne aussi, donc à vous de voir.</p>

<p><strong>Une dernière recommandation : pour monter le cluster, il est important qu'AUCUNE machine virtuelle ne soit présente sur les serveurs (pour éviter les conflits de VMID)</strong>. Si vous récupérez des serveurs existants, il va donc falloir basculer vos machines de droite à gauche pendant la mise en place du cluster. Si vous partez d'une installation neuve, attendez d'avoir monté le cluster pour réimporter vos machines virtuelles.</p>

<p>&nbsp;</p>

## Installation et partionnement

<p>L'installation de l'ISO proxmox se fait toute seule, sans besoin d'intervention. Insérez le disque (ou la clef USB, maintenant supportée), bootez dessus et procédez à l'installation.</p>

<p>Le partionnement de proxmox utilise LVM, et se décompose de la manière suivante :</p>

<ul>
	<li>Une partition physique dédiée au boot, /dev/sda1, d'environ 500Mo</li>
	<li>Une parition physique entièrement formatée en LVM, avec le reste du disque</li>
</ul>

<p>C'est cette deuxième partition qui nous intéresse. Elle utilise LVM, et peut donc être redimensionné par nos soins plus tard. En prenant bien sûr les précautions nécessaires&nbsp;:-)</p>

<ul>
	<li>Un disque logique d'environ 30Go pour /.&nbsp;</li>
	<li>Un disque logique servant de Swap, calculé à partir de la taille de votre RAM</li>
	<li>Un disque logique utilisant tout le reste du disque (moins 4Mo), qui sera monté comme /var/lib/vz et qui servira à stocker vos VM, les backups, etc</li>
</ul>

<p>Pour être plus confortable, notamment au niveau du système, vous pouvez utiliser la technique suivante :</p>

<ul>
	<li>Lors du prompt au boot sur le CD, tapez "debug"</li>
	<li>tapez :&nbsp;<span style="background-color:#000000;"><span style="color:#ffffff;">linux maxroot=100</span></span></li>
</ul>

<p>Cela va configurer la partition système pour utiliser 100Go. Il existe d'autre options, pour plus de détails, voyez&nbsp;<a href="http://pve.proxmox.com/wiki/Debugging_Installation">Debugging Installation</a>.</p>

<p>Pour le reste, c'est correct.</p>

<p>Si vous faites votre partionnement vous-même, veillez à utiliser LVM pour au moins /var/lib/vz. Cela nous permettra d'utiliser les snapshot LVM et de gérer l'espace facilement pour DRBD.</p>

## Personnalisation et astuces

<p>Bon, votre système est installé.</p>

<p>Avant d'attaquer le vif du sujet, je vous propose quelques petits trucs pour faciliter la suite.</p>

### Bash et couleurs

<p>Pour faciliter la gestion de vos 3 serveurs, vous pouvez personnaliser votre bash pour utiliser plusieurs couleurs dans votre connexion SSH.</p>

<p>Pour ce faire, vous pouvez éditer /etc/bash.bashrc</p>

<p><span style="background-color:#000000;"><span style="color:#ffffff;"># nano /etc/bash.bashrc</span></span></p>

<p>Pour ajouter des couleurs, ajoutez ceci à la fin du fichier :</p>

<p style="margin-left: 40px;"># couleurs<br />
<strong><em>C_RED="\[\e[1;31m\]"<br />
C_BLUE="\[\e[1;34m\]"<br />
C_GRAY="\[\e[1;30m\]"<br />
C_WHITE="\[\e[1;37m\]"<br />
C_YELLOW="\[\e[1;33m\]"</em></strong><br />
C_DEF="\[\033[0m\]"<br />
<br />
mUID=`id -u`<br />
MACHINE=`hostname -f`<br />
<br />
if [ "$mUID" = "0" ] ; then<br />
PS1="${<strong>C_RED</strong>}\u${C_DEF}@${<strong>C_RED</strong>}${MACHINE}${C_DEF}:\w${<strong>C_RED</strong>}#${C_DEF} "<br />
PS2="${<strong>C_RED</strong>}&gt;${C_DEF} "<br />
else<br />
PS1="${C_BLUE}\u${C_DEF}@${MACHINE}:\w${C_BLUE}\$ ${C_DEF}"<br />
PS2="${C_BLUE}&gt;${C_DEF} "<br />
fi<br />
<br />
export PS2<br />
export PS1<br />
<br />
case $TERM in<br />
xterm*)<br />
PROMPT_COMMAND='echo -ne "\033]0;${USER}@${MACHINE}: ${PWD}\007"'<br />
echo -ne "\033]0;${USER}@${MACHINE}: ${PWD}\007"<br />
;;<br />
*)<br />
setterm -blength 0<br />
;;<br />
esac</p>

<p>Ce fichier va automatiquement colorer votre nom d'utilisateur et le nom de la machine dans votre connexion SSH.</p>

<p>Si vous êtes connectés avec un autre utilisateur que root, la couleur sera bleue.</p>

<p>Il va également modifier dynamiquement le nom de votre fenêtre SSH selon le dossier où vous êtes. Le but est de ne pas se perdre pour éviter les bêtises&nbsp;;-)</p>

<p>Je vous conseille de changer les parties en <strong>gras</strong> (C_RED) selon les machines (avec les couleurs disponibles en <em><strong>gras italique</strong></em> au dessus)</p>

<p>N'hésitez pas à vous amuser pour trouver les combinaisons de couleurs qui vous plaisent.</p>

<p>Une fois le fichier sauvegardé, vous pouvez activer les modifications pour tester les couleurs avec un :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># exec bash</span></span></p>

<p>Vous pouvez également ajouter à la fin du fichier ces alias, qui peuvent être utiles :</p>

<p style="margin-left: 40px;">alias l='ls -ahlF --color=yes --full-time --time-style=long-iso | more'<br />
alias ll='ls -alhF --color=yes --full-time --time-style=long-iso'<br />
alias cpav='cp -av'</p>

<p>Une fois tout cela configuré selon votre souhait, faites une petite mise à jour de la machine, et redémarrez si nécessaire.</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># aptitude update &amp;&amp; aptitude safe-upgrade -y</span></span></p>

## Configuration des interfaces réseaux

<p>Pour utiliser openVPN et simplifier la gestion des adresses IP publiques entre les serveurs (toujours sympathique), nous allons créer diverses interfaces supplémentaires sur chacun de nos serveurs.</p>

### Architecture interne des serveurs

<p>Après une installation de Proxmox, vous devez disposer d'un bridge virtuel <strong>vmbr0.</strong></p>

<p>Nous allons créer les interfaces suivantes :</p>

<ul>
	<li><strong>vmbr1</strong> : ce bridge permettra la communication en interne au serveur, notamment entre VM. Toutes vos VM devront avoir une interface dans vmbr1 pour discuter entre elles. C'est également cette interface qui permettra la liaison VPN, afin que vos VM puissent discuter entre serveurs, dès fois que vous ayiez plusieurs VM sur chacun de vos serveurs</li>
	<li><strong>vmbr2</strong> : ce bridge un peu particulier permettra la discussion entre les machines "passerelles", qui portent les IP publiques, et les machines ayant besoin d'une connexion entrante ou sortante vers Internet. Ces machines, par exemple en HA, pourront avoir une carte réseau sur vmbr2 et une interface sur vmbr1. Avec une architecture identique sur les 2 serveurs principaux au niveau des VM publiques, les machines basculant en HA retrouveront directement leur connexion</li>
	<li><strong>dummy1</strong> : cette interface "bidon" va permettre d'avoir une interface fixe pour "brancher" openvpn, qui a besoin d'une interface réseau physique pour se lancer correctement au démarrage du serveur</li>
</ul>

<p>Pour que cela soit plus clair, un schéma (le retour) est le bienvenu :</p>

<p><img alt="" src="/images/SchemaInterneVM.png" style="height: 383px; width: 633px;" /><br />
Les adresses IP sont indicatives, mais il est important que le réseau sur vmbr1 soit différent du réseau sur vmbr2. Il est également important que le réseau utilisé sur vmbr2 soit identique sur les 2 serveurs. Cela va permettre de basculer les machines en HA d'un serveur à l'autre sans changer aucune de leurs configurations internes. Il faut également que les passerelles soient équivalentes (sauf l'IP publique bien sûr).</p>

<p>Si vous configurez des routages de ports entre votre passerelle et les machines en HA, il faudra avoir les même sur la passerelle du PRA, même s'ils pointent dans le vide tant que les machines HA ne sont pas sur le PRA.</p>

### Configuration de dummy

<p>Nous allons commencer par créer l'interface dummy nécessaire à OpenVPN. De base, Proxmox peut utiliser une interface dummy pour des usages internes, nous allons donc utiliser une deuxième par précaution :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># echo "options dummy numdummies=2" &gt; /etc/modprobe.d/dummy.conf</span></span></p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># modprobe dummy</span></span></p>

<p>Pour vérifier que les interfaces ont bien été créées, tapez la commande suivante :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># ifconfig -a | grep Link | grep -v inet6</span></span></p>

<p>Qui doit vous renvoyer quelque chose comme ça :</p>

<p style="margin-left: 40px;">dummy0 : Link encap:Ethernet HWaddr xx:xx:xx:xx:xx:xx<br />
dummy1 : Link encap:Ethernet HWaddr xx:xx:xx:xx:xx:xx<br />
eth0 : Link encap:Ethernet HWaddr xx:xx:xx:xx:xx:xx<br />
lo : Link encap:Local Loopback<br />
vmbr0 : Link encap:Ethernet HWaddr xx:xx:xx:xx:xx:xx</p>

### Configuration des interfaces

<p>Nous allons aller toucher au fichier /etc/network/interfaces.</p>

<p>Avant ça, sauvegardez le fichier :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># cpav /etc/network/interfaces /etc/network/interfaces.original</span></span></p>

<p>Puis éditez le :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># nano /etc/network/interfaces</span></span></p>

<p>Ajoutez vmbr1 :</p>

<p style="margin-left: 40px;">auto vmbr1<br />
iface vmbr1 inet static<br />
address XXX.XXX.XXX.XXX<br />
netmask 255.255.255.0<br />
bridge_ports dummy1<br />
bridge_stp off<br />
bridge_fd 0<br />
post-up route add -net 224.0.0.0 netmask 240.0.0.0 dev vmbr1</p>

<p>Dans le schéma de mon exemple, j'utilise 192.168.11.1 pour le serveur principal, 192.168.11.2 pour le PRA, et 192.168.11.3 pour l'arbitre.</p>

<p>Ajoutez vmbr2 :&nbsp;</p>

<p style="margin-left: 40px;">auto vmbr2<br />
iface vmbr2 inet static<br />
address XXX.XXX.XXX.XXX<br />
netmask 255.255.255.0<br />
bridge_ports none<br />
bridge_stp off<br />
bridge_fd 0</p>

<p>Dans le schéma de mon exemple, je prends 192.168.10.253. La passerelle portera par exemple 192.168.10.254.</p>

<p>Pour appliquer les modifications, effectuez un :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># /etc/init.d/networking restart</span></span></p>

<p>Pour vérifier que les nouvelles interfaces fonctionnent correctement, vous pouvez les pinguer :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># ping 192.168.10.253</span></span></p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># ping 192.168.11.1</span></span></p>

### Configuration des services

<p>Pour désactiver l'IPv6, si vous n'en avez pas besoin (pas de troll&nbsp;:-))</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;">#&nbsp;echo "options ipv6 disable=1" &gt; /etc/modprobe.d/disable-ipv6.conf</span></span></p>

<p>Il faut configurer Proxmox pour écouter sur l'interface OpenVPN :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># nano /etc/hosts</span></span></p>

<p>Vérifiez que c'est bien l'adresse de vmbr1 qui est configurée :</p>

<p style="margin-left: 40px;">127.0.0.1 localhost.localdomain localhost<br />
xxx.xxx.xxx.xxx serveurprincipal.mon-cluster.vpn pra pvelocalhost<br />
[...]</p>

<p>Où xxx.xxx.xxx.xxx est l'adresse que vous avez indiquée pour <strong>vmbr1</strong> (pour moi 192.168.11.1)</p>

<p>Pour avoir des logs propres au démarrage (on sait jamais)</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># nano&nbsp;/etc/default/bootlogd&nbsp;</span></span></p>

<p>Et modifiez :</p>

<p style="margin-left: 40px;">BOOTLOGD_ENABLE=Yes</p>

## Installation d'OpenVPN

<p>On est parti ! Tout ce qui était avant va vous servir&nbsp;:-)</p>

<p>Installons OpenVPN pour commencer :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># aptitude install openvpn</span></span></p>

<p>Faites ceci sur tous vos serveurs.</p>

### Préparation des certificats

<p>Sur le serveur PRA, le serveur principal d'OpenVPN, nous allons générer les certificats serveurs et clients.</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># cpav /usr/share/doc/openvpn/examples/easy-rsa/2.0/ /etc/openvpn/easy-rsa/<br />
<br />
# cd /etc/openvpn/easy-rsa/<br />
<br />
# cpav vars vars.original</span></span></p>

<p>Editez le fichier vars, tout à la fin du fichier, pour modifier les variables globales. Ainsi votre génération de clef se fera automatiquement. Sinon, vous devrez indiquer les informations à chaque génération.</p>

<p>Puis :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># source ./vars</span></span></p>

<p style="margin-left: 40px;">NOTE: If you run ./clean-all, I will be doing a rm -rf on /etc/openvpn/easy-rsa/keys</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># ./clean-all</span><br />
<span style="background-color:#000000;"># ./build-dh</span></span></p>

<p style="margin-left: 40px;">Generating DH parameters, 1024 bit long safe prime, generator 2<br />
This is going to take a long time<br />
........................................................+......<br />
..+............+...............................................<br />
...............................................................<br />
...+............................................++*++*++*</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># ./pkitool --initca</span></span></p>

<p style="margin-left: 40px;">Using CA Common Name: Mon organisation CA<br />
Generating a 1024 bit RSA private key<br />
...++++++<br />
................++++++<br />
writing new private key to 'ca.key'<br />
-----</p>

### Générez la clef serveur

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># ./pkitool --server server</span></span></p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># openvpn --genkey --secret ./keys/ta.key</span></span></p>

### Générez les clefs clients

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># ./pkitool clientPrincipal</span></span></p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># ./pkitool clientArbitre</span></span></p>

<p>Vous pouvez également en profiter pour générer d'autres clefs clients si vous souhaitez vous connecter vous-même au VPN, pour sécuriser à fond vos accès SSH par exemple.</p>

### Sauvegarde des clefs

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># cd /etc/openvpn/easy-rsa/keys/</span><br />
<br />
<span style="background-color:#000000;"># tar cvzf /root/server-keys.tar.gz server.crt server.key ca.crt dh1024.pem ta.key</span><br />
<br />
<span style="background-color:#000000;"># tar cvzf /root/clientPrincipal-keys.tar.gz clientPrincipal.crt clientPrincipal.key ca.crt ta.key</span></span></p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># tar cvzf /root/clientArbitre-keys.tar.gz clientArbitre.crt clientPrincipal.key ca.crt ta.key</span></span></p>

### Configuration des serveurs

<p>Pour mettre en place le fameux triangle OpenVPN permettant un quorum solide, nous allons devoir configurer à la fois le serveur principal ET le serveur PRA comme serveurs OpenVPN.</p>

<p>La différence se jouera dans les fichiers de configurations des clients.</p>

<p>Sur chacune des machines, importez le fichier de configuration par défaut :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># cd /etc/openvpn/<br />
<br />
# zcat /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz &gt; server.conf<br />
<br />
# cpav server.conf server.conf.original</span></span></p>

<p>Pour une configuration idéale (d'après mes tests de robustesse personnels), le fichier doit ressemble à ça sans ses commentaires :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># cat server.conf | grep -v -e '^;' | grep -v -e '^#' | grep -v -e '^$'</span></span></p>

<p style="margin-left: 40px;"><strong>local IP_PUBLIQUE_SERVEUR</strong><br />
<strong>port 4450</strong><br />
proto tcp<br />
dev tap0<br />
ca ca.crt<br />
cert server.crt<br />
key server.key # This file should be kept secret<br />
dh dh1024.pem<br />
<strong>server-bridge XXX.XXX.XXX.XXX 255.255.255.0 YYY.YYY.YYY.YYY TTT.TTT.TTT.TTT</strong><br />
client-to-client<br />
duplicate-cn<br />
keepalive 10 120<br />
tls-auth /etc/openvpn/ta.key 0 # This file is secret<br />
comp-lzo<br />
max-clients 4<br />
user nobody<br />
group nogroup<br />
persist-key<br />
persist-tun<br />
status openvpn-status.log<br />
log-append openvpn.log<br />
verb 3<br />
mode server<br />
tls-server<br />
script-security 2<br />
up "/etc/openvpn/up.sh"<br />
down "/etc/openvpn/down.sh"</p>

<p>Les choses à modifier selon le serveur sont en gras :</p>

<ul>
	<li>local IP_PUBLIQUE_SERVEUR : remplacez par l'adresse IP publique de votre serveur, pour écouter en VPN sur cette adresse. Cette adresse est a priori différente selon le serveur !&nbsp;;-)</li>
	<li>Le port d'écoute. Par défaut 1194, mais il est conseillé de le changer pour éviter d'être reconnu direct comme un VPN</li>
	<li>XXX.XXX.XXX.XXX : remplacez par l'adresse privée du <strong>vmbr1</strong> du serveur. Attention à ne pas vous tromper !</li>
	<li>YYY.YYY.YYY.YYY : cette IP est le début de la plage allouée pour OpenVPN. Pour ma part, je prend 4 clients possibles. Les 2 serveurs et 2 autres (pour vos connexions de gestion par exemple)</li>
	<li>TTT.TTT.TTT.TTT : Cette IP est la fin de la plage allouée commencée par YYY.YYY.YYY.YYY. Cette plage doit contenir les IP de vos serveurs, mais comme ceux-ci sont en IP fixe, pas de risque d'allocation se marchant dessus.</li>
</ul>

### Scripts up.sh et down.sh (pour les serveurs)

<p>Ces scripts vont permettre d'ajouter l'interface OpenVPN (tap0) au bridge vmbr1 à chaque démarrage du serveur VPN. Et bien sûr de le retirer à l'arrêt du serveur VPN (faisons les choses bien&nbsp;:-))</p>

<p>Ces scripts seront sensiblement différents pour les clients, nous verrons ça plus bas.</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># nano /etc/openvpn/up.sh</span></span></p>

<p style="margin-left: 40px;">#!/bin/bash<br />
/sbin/ifconfig vmbr1 promisc<br />
/sbin/ifconfig tap0 up promisc<br />
/usr/sbin/brctl addif vmbr1 tap0</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># nano /etc/openvpn/down.sh</span></span></p>

<p style="margin-left: 40px;">#!/bin/bash<br />
/usr/sbin/brctl delif vmbr1 tap0<br />
/sbin/ifconfig tap0 down -promisc<br />
#/sbin/ifconfig vmbr1 -promisc</p>

### Installation des clefs serveurs

<p>Chacun des serveur devra disposer du set de clef serveur que nous avons généré plus haut. Sans quoi, ils n'accepteront pas les connexions du client.</p>

<p>Physiquement, le client ne sera connecté qu'à un serveur à la fois, mais il faut que les 2 serveurs tournent en permanence pour que le client puisse basculer facilement en cas de souci sur le PRA.</p>

<p>Sur le PRA, où vous avez généré les clefs, il suffit de les placer dans le dossier openVPN puis de les extraire :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># cd /etc/openvpn</span></span></p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># tar xvzf /root/</span></span><span style="color:#ffffff;"><span style="background-color:#000000;">server-keys.tar.gz</span></span></p>

<p>Sur le serveur principal, il va d'abord falloir envoyer les clefs via scp :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># scp&nbsp;</span></span><span style="color:#ffffff;"><span style="background-color:#000000;">/root/</span></span><span style="color:#ffffff;"><span style="background-color:#000000;">server-keys.tar.gz root@IP_PUBLIQUE_SERVEUR:</span></span></p>

<p><strong>Note : il n'est pas très propre d'utiliser root pour le transfert, mais c'est malheureusement la technique la plus simple sur Proxmox, où quasi-tout doit se faire en root. Vous pouvez cependant créer un autre utilisateur qui ne servira que pour les transferts. De toute façon, le cluster discute avec le compte root, donc le SSH sur le port 22 en root devra au moins être autorisé dans le tunnel OpenVPN. Je vous laisse gérer votre niveau de sécurité&nbsp;:-)</strong></p>

<p>Puis extraire les clefs :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># cd /etc/openvpn</span></span></p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># tar xvzf /root/</span></span><span style="color:#ffffff;"><span style="background-color:#000000;">server-keys.tar.gz</span></span></p>

<p>Une fois ceci fait, vous pouvez redémarrer OpenVPN sur les 2 serveurs :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># /etc/init.d/openvpn restart</span></span></p>

<p>Normalement, il doit démarrer sans erreurs des deux côtés. Les logs s'écrivent dans /etc/openvpn/openvpn.log</p>

<p>Un démarrage réussit doit se terminer par&nbsp;</p>

<p style="margin-left: 40px;">Initialization sequence completed</p>

<p>Sinon, vérifiez bien les adresses IP que vous avez indiquée (problème de bind) et la synthaxe des paramètres dans le fichier de conf (problème de "unknown parameter")</p>

### Configuration des clients

<p>A ce stade, nous avons uniquement les serveurs VPN qui tournent. Il va vous falloir configurer les clients qui s'y connectent.</p>

<p>Au nombre des clients, nous avons donc l'arbitre, et le serveur principal qui je le rappelle est à la fois client ET serveur.</p>

<p>Nous allons configurer l'arbitre pour qu'il tente en premier lieu de se connecter au PRA, et s'il échoue qu'il tente de se connecter au serveur principal. Nous allons également indiquer dans sa configuration que si sa connexion saute, il doit réessayer immédiatement de se reconnecter.</p>

<p>Par contre, le serveur principal n'essaiera de se connecter qu'au PRA.</p>

<p>Voici le fichier de configuration. La spécificité de l'arbitre est en <span style="color:#ff0000;">rouge</span> :</p>

<p style="margin-left: 40px;">client<br />
<strong>dev tap1</strong><br />
proto tcp<br />
<strong>remote IP_PUBLIQUE_PRA PORT_VPN<br />
<span style="color:#ff0000;">remote IP_PUBLIQUE_SERVEUR_PRINCIPAL PORT_VPN</span></strong><br />
resolv-retry infinite<br />
nobind<br />
persist-key<br />
persist-tun<br />
ca ca.crt<br />
<strong>cert CLIENT.crt<br />
key CLIENT.key</strong><br />
ns-cert-type server<br />
tls-auth ta.key 1<br />
comp-lzo<br />
verb 3<br />
<strong>log-append openvpnclient.log</strong><br />
script-security 2<br />
<strong>up "/etc/openvpn/upclient.sh"<br />
down "/etc/openvpn/downclient.sh</strong>"<br />
<strong>keepalive 30 120</strong><br />
float</p>

<p>Les paramètres à adapter sont en <strong>gras</strong> :</p>

<ul>
	<li>dev tapX : c'est l'interface que crééra openvpn pour sa connexion. Il est important de ne pas mettre tap0, qui est déjà utilisé pour le serveur VPN sur le serveur principal. Pour uniformiser, mieux vaut utiliser la même interface sur les 2 clients</li>
	<li>remote .... : c'est là que vous indiquez sur quel serveur se connecter. Indiquez l'adresse IP du PRA ainsi que le port que vous avez choisi. Sur l'arbitre, rajoutez une ligne avec l'adresse IP du serveur principal. OpenVPN essaiera toujours la première ligne, puis si elle échoue il passera à la deuxième.</li>
	<li>cert, key : c'est là que vous définirez les certificats et les clefs à utiliser pour la connexion client (clientPrincipale ou clientArbitre) que nous allons rapatrier via scp.</li>
	<li>log-append : &nbsp;indique le fichier dans lequel les logs de connexion seront écrits. Indiquez un nom de fichier différent de celui du serveur pour avoir des logs bien séparés</li>
	<li>up, down : les fichiers up.sh et down.sh sont différents entre les serveurs et les clients, donc indiquez là aussi des noms différents ce ceux dans le fichier de conf du serveur</li>
	<li>keepalive 30 120 : cette directive va indiquer aux clients qu'il doivent tenter de relancer leur connexion si jamais elle tombe (On ping toutes les 30s, on redémarre au bout de 120s si pas de réponse). Dans le cas du serveur principal, il essaiera en boucle de se reconnecter sur le PRA. Dans le cas de l'arbitre, il alternera entre le PRA et le serveur principal jusqu'à ce qu'une connexion réussisse</li>
</ul>

### Scripts upclient et downclient

<p>Les fichiers up.sh et down.sh sont différents pour les clients. Comme le serveur principal possède déjà up.sh et down.sh, on les nomme upclient.sh et downclient.sh pour qu'ils ne se marchent pas dessus (quelle imagination !&nbsp;\o/)</p>

<p>Donc, upclient.sh :</p>

<p style="margin-left: 40px;">#!/bin/bash<br />
/sbin/ifconfig vmbr1 promisc<br />
/sbin/ifconfig tap1 up promisc<br />
/usr/sbin/brctl addif vmbr1 tap1<br />
/sbin/ifconfig tap1 0</p>

<p>Et downclient.sh :</p>

<p style="margin-left: 40px;">#!/bin/bash<br />
/sbin/ifconfig vmbr1 -promisc</p>

<p>La principale différence est le nom des interfaces à bridger (tap1 dans mon cas). Le down client ne doit également pas sortir le tap1 du bridge, sinon il risque de générer une erreur qui va bloquer la tentative de connexion en boucle du client. Et donc rendre inefficace le mode de secours. (Un petit souci de gestion de bridge avec OpenVPN vis à vis de tous les bridges/tap qu'on utilise ici...)</p>

### Récupération des clefs clients

<p>Bien, il ne reste plus qu'à transmettre les clefs openvpn à qui de droit. Depuis le PRA qui les a générées, envoyez les tar.gz grâce à scp :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># scp&nbsp;/root/clientPrincipal-keys.tar.gz root@IP_PUBLIQUE_SERVEURPRINCIPAL:</span></span></p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># scp&nbsp;/root/clientArbitre-keys.tar.gz&nbsp;root@IP_PUBLIQUE_ARBITRE:</span></span></p>

<p>Puis sur chaque serveur, extrayez les clefs dans /etc/openvpn :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># cd /etc/openvpn</span></span></p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># tar xvzf /root/client{Principal,Arbitre}-keys.tar.gz</span></span></p>

<p>Et enfin, redémarrez openvpn.</p>

<p>La connexion va s'établir. Là encore, les logs s'écrivent dans /etc/openvpn, dans le fichier openvpnclient.log spécifé dans le fichier de configuration.</p>

<p>Si tout se passe bien, vous pourrez y voir apparaitre un "Initialization sequence completed".</p>

<p>C'est également dans ce fichier que vous pouvez vérifier si la reconnexion se fait bien.</p>

## Tests du triangle openvpn

<p>Il est important de tester votre système de connexion/reconnexion.</p>

<p>A l'étape précédente, la connexion de base a dû bien fonctionner. Sinon, faites en sorte que ce soit le cas. Si jamais vous avez des soucis particuliers, faites m'en part dans les commentaires&nbsp;:-)</p>

<p>Pour tester, effectuez quelques ping sur les adresse vmbr1 de chaque serveur :</p>

<ul>
	<li>Sur le serveur principal, pinguez le vmbr1 du PRA et de l'arbitre</li>
	<li>Sur le PRA, pinguez le vmbr1 du serveur principal et de l'arbitre</li>
	<li>sur l'arbitre, pinguez le vmbr1 du serveur principal et du PRA</li>
</ul>

<p>&nbsp;</p>

<p>Pour tester que la reconnexion fonctionne bien, nous allons essayer de couper le serveur VPN du PRA et voir comment tout cela se comporte.</p>

<p>Ouvrez 3 connexions SSH, une par serveur.</p>

<ul>
	<li>Sur l'arbitre et le serveur principal, lancez une surveillance du client OpenVPN :</li>
</ul>

<p style="margin-left: 40px;"><span style="color:#ffffff;"><span style="background-color:#000000;"># less /etc/openvpn/openvpnclient.log</span></span></p>

<p style="margin-left: 40px;">Puis effectuez la combinaison de touche "maj + F" qui va permettre une mise à jour en temps réel du fichier. Vous verrez ainsi tout ce qui s'y écrit</p>

<ul>
	<li>Sur le PRA, coupez openvpn avec un&nbsp;</li>
</ul>

<p style="margin-left: 40px;"><span style="color:#ffffff;"><span style="background-color:#000000;"># /etc/init.d/openvpn stop</span></span></p>

<p>Normalement, vous devriez rapidement voir dans les fichiers de log que la connexion s'est interrompue. Sur le serveur Principal, la connexion va tenter en boucle de s'établir à nouveau.</p>

<p>Sur l'arbitre, vous devriez voir rapidement une tentative de connexion sur le serveur principal, qui doit réussir.</p>

<p>Là encore, vous pouvez vérifier avec quelques ping que tout va bien.</p>

<p>Relancez ensuite le serveur VPN sur le PRA. Vous devez observer que le serveur principal revient s'y connecter.</p>

<p><strong>A ce moment là, l'arbitre est connecté sur le serveur principal. Il ne va pas essayer de se reconnecter sur le PRA sauf si le serveur principal tombe.</strong></p>

<p>Pour achever nos tests, coupez le serveur VPN sur le serveur principal, ce qui devrait provoquer la reconnexion de l'arbitre sur le PRA.</p>

<p>Si tout ça fonctionne, on commence à être vraiment bon !&nbsp;:-)</p>

## Mise en place du cluster

<p>Avant de mettre en place le cluster, nous allons vérifier que le multicast passe bien dans le VPN.</p>

<p>Pour ce faire, sur chaque machine, installez l'outil suivant :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># aptitude install ssmping</span></span></p>

<p>Ensuite, nous allons tester les serveurs 2 par 2. L'un essaiera de joindre l'autre en multicast, pendant que la cible écoutera les connexions.</p>

<p>Bon je vais pas écrire toutes les combinaisons possibles pour les tests. Sachez qu'il vous faut lancer sur un serveur pour le mettre en écoute :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;">#&nbsp;ssmpingd</span></span></p>

<p>Puis depuis un autre, lancez le test :</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># asmping 224.0.2.1 IP_SERVEUR_TEST</span></span></p>

<p>Vous devez voir passer sur le serveur en écoute des messages de ce type :</p>

<p style="margin-left: 40px;">asmping joined (S,G) = (*,224.0.2.234)<br />
pinging IP_SERVEUR_TEST from IP_SERVEUR_SOURCE<br />
unicast from IP_SERVEUR_TEST, seq=1 dist=0 time=38.699 ms<br />
multicast from IP_SERVEUR_TEST, seq=1 dist=0 time=76.405 ms<br />
unicast from IP_SERVEUR_TEST, seq=2 dist=0 time=38.802 ms<br />
multicast from IP_SERVEUR_TEST, seq=2 dist=0 time=76.238 ms</p>

<p>Les lignes importantes à repérer sont celles du multicast,&nbsp;<strong>que vous devez impérativement constater pour le bon fonctionnement du cluster !</strong></p>

<p>C'est également le bon moment pour vérifier le fichier /etc/hosts.</p>

<p>Vous devez avoir indiqué l'adresse IP du vmbr1. C'est l'adresse dont se sert Proxmox comme information lors du contact avec les autres membres du cluster.</p>

### Création du cluster

<p>Pour le cluster, nous allons effectuer sa création depuis le PRA, puis nous allons y ajouter les 2 autres serveurs.</p>

<p><strong>Je vous rappelle qu'il ne doit y avoir aucune VM sur le serveur principal et sur l'arbitre ! </strong>Si votre système a déjà des VM en fonctionnement, migrez les temporairement sur le PRA le temps de créer le cluster. Cette limitation a pour but de s'assurer qu'aucun VMID ne sera en conflit au sein du cluster.</p>

<p>Le nom du cluster est important, &nbsp;dans le sens où une fois le cluster créé, vous ne pourrez plus changer ce nom. Choisissez le bien !</p>

<p>Sur le PRA :&nbsp;</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># pvecm create NOM_DU_CLUSTER</span></span></p>

<p>qui doit vous renvoyer :</p>

<p style="margin-left: 40px;"><br />
Generating public/private rsa key pair.<br />
Your identification has been saved in /root/.ssh/id_rsa.<br />
Your public key has been saved in /root/.ssh/id_rsa.pub.<br />
The key fingerprint is:<br />
xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx root@proxmox-server-1<br />
The key's randomart image is:<br />
+--[ RSA 2048]----+<br />
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |<br />
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |<br />
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; |<br />
| &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; | &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />
| &nbsp; &nbsp; &nbsp;&nbsp; . &nbsp; So &nbsp;&nbsp; o &nbsp; &nbsp; &nbsp; &nbsp;|<br />
| . &nbsp; &nbsp; &nbsp;.. &nbsp; &nbsp; &nbsp;.O &nbsp; &nbsp;o &nbsp; &nbsp; |<br />
<br />
| .. &nbsp; . &nbsp;&nbsp; o* &nbsp; &nbsp; = &nbsp; &nbsp; &nbsp; &nbsp; |<br />
|&nbsp; &nbsp; +o &nbsp; &nbsp; &nbsp; o.=.. &nbsp; &nbsp; &nbsp; &nbsp;|<br />
| . &nbsp; &nbsp; +E &nbsp;* &nbsp;o &nbsp; &nbsp;o . &nbsp; |<br />
+----------------------+<br />
Restarting pve cluster filesystem: pve-cluster[dcdb] crit: unable to read cluster config file '/etc/cluster/cluster.conf' - Failed to open file '/etc/cluster/cluster.conf': No such file or directory<br />
[dcdb] notice: wrote new cluster config '/etc/cluster/cluster.conf'<br />
[dcdb] crit: cman_tool version failed with exit code 1#010<br />
.<br />
Starting cluster:<br />
Checking if cluster has been disabled at boot... [ OK ]<br />
Checking Network Manager... [ OK ]<br />
Global setup... [ OK ]<br />
Loading kernel modules... [ OK ]<br />
Mounting configfs... [ OK ]<br />
Starting cman... [ OK ]<br />
Waiting for quorum... [ OK ]<br />
Starting fenced... [ OK ]<br />
Starting dlm_controld... [ OK ]<br />
Unfencing self... [ OK ]</p>

<p>Puis, sur chacun des autres serveurs :</p>

<p># pvecm add IP_VMBR1_PRA</p>

<p>qui doit vous renvoyer à peu près la même chose qu'au dessus.</p>

### Etat du cluster

<p>La commande pvecm vous permettra d'avoir plusieurs infos sur le cluster.</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># pvecm n</span></span></p>

<p>vous donnera un aperçu des nodes (date de connexion, état, etc)</p>

<p><span style="color:#ffffff;"><span style="background-color:#000000;"># pvecm s</span></span></p>

<p>vous donnera des informations sur le cluster lui-même (état du qorum, nombre de node, nombre de vote, etc)</p>

<p>Nous verrons plus tard des commandes plus spécifiques au HA.</p>

## Bon, je crois que j'ai rien oublié

<p>Et bien voila qui conclut la création de notre système de cluster !</p>

<p>A la fin de ce tutoriel, vous avez normalement un cluster de 3 nodes, donc avec un quorum qui fonctionne, et des paritions manipulables facilement.</p>

<p>Vous êtes prêts à mettre en place le système de disque répliqué, puis le HA lui-même.</p>

<p>Ces deux étapes seront le sujet des prochains articles.</p>

<p>En attendant, n'hésitez pas à me poser des questions dans les commentaires !</p>

<p>&nbsp;</p>
