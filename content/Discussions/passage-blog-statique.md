Title: Passage à un système de blog statique
subtitle: (basé sur Pélican)
Date: 2017-07-04 09:10
Modified: 2017-07-04 09:10
Category: Discussion
Tags: hébergement, blog
keywords: hébergement, pelican, blog
Slug: passage-blog-statique
Authors: Victor
Status: published
Summary: Aujourd'hui, c'est la mise en production du nouveau blog (ou du blog nouveau)

[TOC]

# Un blog pardon quoi ?
Bonjour à toutes et à a tous !

Un système de blog statique on vous dit !

C'est un concept où le blog (ou le site) n'est constitué que de fichiers entièrement statiques.  
Plus de php, de bases de données ou autre scripts exécutés côté serveur, on est sur du 100% statique, servi par un serveur web, et c'est tout.

Des images, de l'html, du css et éventuellement un peu de javascript pour faire joli.

# Mais pourquoi ?

## De l'abandon de PluXML

Et oui, ça faisait un moment que je souhaitais mettre de côté mon ancien moteur de blog.  
Il m'a fidèlement servi pendant des années, et je tiens à remercier la communauté [PluXML](http://pluxml.org/) pour ce CMS très léger et parfaitement fonctionnel.

Néanmoins, le système de captcha pour les commentaires (et malgré les plugins) laissait à désirer et je passait trop de temps à virer les spams qui passaient dans les mailles du filet.

De plus, la gestion des mises à jour, bien que de mieux en mieux intégrée, reste comme pour tous les CMS assez critique. Les failles sont en général dangereuses (injection de script, réécriture d'articles ou de commentaires, ...) et jamais faciles à suivre, parfois pas simple à corriger. Elles peuvent générer des bugs difficile à repérer, etc...

En outre, il utilise php (comme la plupart des CMS), et bien qu'il n'utilise pas de bases de données au sens propre (stockage plat dans de l'xml), il reste mine de rien consommateur en IO et en ressources, surtout avec ma notoriété croissante (ahah)

Plus sérieusement, PluXML est très bien et ce sont principalement les arguments des spam commentaires et de la gestion des mises à jour qui ont fait pencher la balance.

## Des avantages du blog statique

Le blog statique se démarque sur 2 points :  

  - Il est statique
  - Le moteur qui le génère n'est pas accessible pour un tiers.

Le premier point parait trivial, mais en fait pas tant que ça.  
Le 2eme point demande également plus d'explication

### Pas de script côté serveur
En effet, le fait d'avoir un site entièrement statique rend l'hébergement ô combien plus simple. Mine de rien, la gestion de php (et de ses mises à jour) ou d'une base de donnée reste consommateur en temps, demande des downtime pour les maintenances, etc.

De plus, un site qui exécute des scripts (php notamment) sur le serveur consomme des ressources serveur, et surtout est un point d'entrée éventuel pour un hack.  
Bien que ça se passe la plupart du temps très bien, si jamais quelqu'un arrive à exécuter un fichier de son cru sur le serveur (injection de fichier, faille, etc), il peut alors faire tout ce qu'il veut, de transformer le serveur en zombie de botnet à miner du [bitoin](https://fr.wikipedia.org/wiki/Bitcoin) (ou des [Ether](https://fr.wikipedia.org/wiki/Ethereum), on commence à en voir), envoyer des spam, ou simplement surveiller ce qui s'y passe.

Dans tous ces cas, votre serveur est compromis, c'est mort, foutu, en général repartir de zéro est plus tranquillisant pour l'esprit.  
Ca ne m'est jamais arrivé personnellement, mais dans mon boulot j'en vois régulièrement, et c'est suffisant pour que je ne tienne pas à le subir à la maison :-)

**Avec un blog statique, rien de tout ça.** Il n'y a strictement rien d'exécuté côté serveur, les fichiers sont statiques (d'où le nom) et donc sont envoyés tels quels au navigateur du visiteur, dans l'idéal ils ne sont même pas lus par le serveur (autre que pour les envoyer of course), et encore moins interprétés.

Bien sûr ce n'est pas parfait, mais déjà il y a du mieux.

### Le moteur est extérieur au site
Générer ces fichiers statiques ne se fait pas par magie.

En effet, il est possible d'écrire soit même son CSS, son html, et remplir chaque page à la main. Soyons honnêtes, je ne suis pas développeur web et l'html/css, disons que ça me passionne pas.  
Du coup, plusieurs moteurs existent qui permette de générer ces fichiers statiques à partir de templates, de choisir des thèmes, etc.

Et donc, le moteur qui génère tous ces fichiers est extérieur au site. C'est à dire qu'il n'est pas exposé publiquement.

Dans le cas de php par exemple, ce sont les visiteurs qui lancent les scripts php et provoquent la génération des pages, avec les inconvénients vu ci-dessus.  
Dans le cas du site statique, c'est le rédacteur qui va générer les pages une fois, lorsqu'il le souhaite (on parle de compilation comme pour un logiciel) et ensuite elles ne bougent plus.

Le premier avantage est en terme de ressources, car le site est généré une fois et on peut le faire n'importe quand (la nuit, sur un serveur dédié à ça) pour que ça n'impacte pas le serveur exposé au public.

Le 2eme avantage touche aux mises à jour du moteur et aux failles associées. En effet, le moteur n'est plus exposé au public !  
Donc, même si des failles apparaissent, leur exploitation est beaucoup plus difficile, voire impossible, pour un hacker puisqu'il n'a tout simplement pas accès au système.

Et en plus, comme le site statique vit sa vie indépendamment du moteur, les mises à jour de ce dernier n'impliquent pas de maintenance ou de coupure du site, ce qui est appréciable :-)

# Ok ça rox, pourquoi ces sites n'ont pas conquis le monde ??
Alors ne mentons pas, il y a des désavantages.

Le principal étant que... le site est statique (Merci [Capitaine Obvious](https://fr.wiktionary.org/wiki/Captain_Obvious) !)  
Ça implique qu'il n'y a strictement rien pour interagir avec les visiteurs.

En effet, pas d'intéraction côté serveur, c'est pas de commentaires, pas de livre d'or, aucun moyen pour un visiteur de choisir ou configurer quelque chose qui serait enregistré dans une base de données et réutilisé plus tard (panier, compte, moyen de paiement, adresse courriel, ...)

On voit tout de suite que ça pose certains problèmes pour la plupart des sites du marchés, et notamment les sites qui fournissent un service (vente de produit, vente de vos informations privées et autres)

Pour un blog, par contre, c'est nickel. L'intéraction est minimale, et on peut gérer la plupart des problèmes en les contournant.

**Sauf sur un point.**

## De la gestion des commentaires
LES COMMENTAIRES. Tout bon blog sérieux se doit d'en avoir, sinon c'est trop la honte on a peur des retours des gens sur nos articles lolilol.

Non réellement, j'aime bien les commentaires parce que ça permet un échange avec mes lecteurs, et moi j'aime bien échanger, apprendre et transmettre, m'améliorer et améliorer.  
C'est même pour ça que j'ai fais un blog, alors...

Et du coup les commentaires **sont** un problème sur un site statique, puisqu'il ne permet pas d'intéragir.

Des solutions existent, et la solution la plus répandue est l'utilisation de [disqus](https://disqus.com/), un site qui ne fait que des commentaires. (c'est présenté avec des fioritures autour mais en gros c'est ça)  
Vous l'incluez dans votre site avec un peu de javascript, et il apparait bien intégré, le visiteur peut voir les commentaires, y répondre et en ajouter, c'est bô.   

Sauf que ce n'est pas sur votre serveur, c'est le site de Disqus qui est affiché, votre navigateur parle à Disqus, et vos commentaires sont enregistrés chez Disqus.  
Ça me dérangeait un peu, comme concept.

Alors, j'ai pris une autre solution, certes moins élégante et plus "brute de décoffrage".

**Pour commenter, envoyez moi un courriel.**

Alors je sais, vous allez me dire, "Queeeeouuuââââ ? Que raconte-t-il ce vil faquin ? Envoyer des courriels en 2017 ??"

Alors d'une part, vous devez dans tous les cas fournir une adresse de messagerie pour poster un commentaire, où que ce soit, envoyer un vrai courriel ne coute donc pas cher.

D'autre part, j'ai quand même fait en sorte de simplifier les choses, faut pas déconner.

En bas des articles, dans la section "Comments" (oui je travaille à la traduction), vous trouverez un espace de commentaire tout ce qu'il y a de plus classique.

Écrivez votre commentaire, comme vous le souhaitez, comme d'habitude. **La seule différence** c'est que quand vous cliquerez sur envoyer, ça vous fera un joli mailto:, qui ouvrira votre client de messagerie par défaut, et proposera d'envoyer le commentaire par courriel, tout bien formaté comme il faut. Si votre client est bien configuré, il suffira d'appuyer sur "Envoyer", et c'est tout !

Ensuite, je fais un peu de modération (parce qu'on est quand même pas à l'abri d'un robot un peu trop malin...), et hop votre commentaire sera publié.  
Exactement comme sur mon ancien blog, et comme sur la majorité des sites présents sur Internet.

Et comme ça, votre commentaire restera chez moi, sur mon serveur, dans mon salon, de même que tout ce qui touche au blog.

**Elle est pas belle la vie ?**

# Et sinon, la technique ?
Ah oui, comme ce blog se veut quand même un peu technique, parlons tuyauterie.

Ce blog est désormais propulsé par [Pélican](https://blog.getpelican.com/), sur une base de python.

Le thème graphique du blog est inspiré par [Elegant](http://oncrashreboot.com/elegant-best-pelican-theme-features) de [http://oncrashreboot.com](http://oncrashreboot.com).

Il est géré et versionné via [un dépôt git](https://git.lecygnenoir.info/LecygneNoir/blog.victor-hery.com). Un peu d'intégration continue maison pour publier automatiquement a été ajoutée par dessus.  
Vous pouvez librement cloner le dépôt si vous voulez récupérer des morceaux de code, tout est sous licence [Creative Common](https://creativecommons.org/licenses/?lang=fr). Je vous demanderai juste de citer le site d'origine sous la forme : [https://blog.victor-hery.com](https://blog.victor-hery.com)

Vous pouvez aussi vous inscrire au dépôt (ou créer des tickets) si vous voulez envoyer des pull request, pour améliorer des articles ou corriger des fautes d'orthographe par exemple.

Le blog est rédigé en [MarkDown](https://fr.wikipedia.org/wiki/Markdown)

Bien sûr, tous les articles et leurs commentaires ont été importés.

Cet article se voulant une annonce, je reste volontairement flou sur la partie technique (ça fait déjà une sacrée annonce à lire)  
Si vous voulez plus de détails, ou que je fasse un article pour détailler la couche technique que j'utilise, n'hésitez pas à commenter ;-)

Dites moi ce que vous pensez de ce nouveau fonctionnement, j'espère qu'il vous plaira, et à bientôt pour de nouveaux articles !
