Title: Marre du filtrage "sécurité" des hébergeurs !
subtitle: ; On rale
Date: 2012-09-05 18:57
Modified: 2012-09-05 18:57
Category: Discussion
Tags: online, hébergement, sécurité, blocage
keywords: online, hébergement, sécurité, blocage
Slug: marre-filtrage-securite-hebergeurs
Authors: Victor
Status: published

[TOC]

## Votre hébergeur roxx !

<p>Auneffet, ses serveurs HP et DELL vous donne accès à une carte ILO ou DRAC, qui permet de faire plein de choses sympas pour gérer votre serveur à un niveau supérieur.&nbsp;</p>

<p>Entre autre, grâce à ces cartes, vous pouvez décider de booter votre serveur sur une iso de votre cru, qui est bien sûr votre distribution Linux préférée trop bien que vous pouvez plus vous en passer.</p>

<p>(Ou alors, vous voulez juste installer un système en gérant correctement le partitionnement, ce qui est pas encore gagné via les interfaces web...).</p>

<p>&nbsp;</p>

<p>Sauf que bon, charger une image iso de 600Mo depuis chez vous, avec votre ADSL de merde by "******", ça rend tout de suite le truc moins sexy.</p>

<p>C'est alors que tout frétillant vous vous dites "Mais oui" !</p>

<p>Car oui ! vous avez un autre serveur dédié chez le même hébergeur (car décidément vous le kiffez à mort).</p>

<p>Quoi de plus simple alors que d'installer une petite machine virtuelle sur le deuxième serveur, avec une interface graphique pour lancer la magnifique applet (<a href="http://fr.wikipedia.org/wiki/Appliquette">appliquette</a>) java permettant de faire votre bonheur ?</p>

## Et ouais. Sauf que non

<p>[MODE GROGNE : ON]</p>

<p>Car, pour des raisons plus ou moins fallacieuses, vous allez vite déchanter.</p>

<p>Votre hébergeur, dans un souci constant de pourvoyer à votre sécurité (Ca fait partie du contrat ??) applique un filtrage sur ce qui se passe dans son réseau.</p>

<p>Manque de bol, il considère ses propres adresses IP comme pas de confiance, et donc bloque tout accès à sa propre console depuis une machine de son propre réseau. (On croit rêver...)</p>

<p>Comment font les techniciens pour intervenir ? Mystère... Sans doute un VPN/proxy installé chez un concurrent est-il le seul moyen pour les pauvres hères de travailler dans des conditions décentes.</p>

<p>Quoiqu'il en soit, après avoir installé votre distribution GNU/Linux avec une interface graphique GNU/Linux, avoir fait la configuration de NAT qui va bien (car votre hébergeur <strike>est toujours un gros extrémiste</strike> sécurise son réseau, <a href="#Et en plus...">voir plus bas</a>) vous chargez votre navigateur libre préféré, et vous vous mangez un méchant "403, Forbidden". Autrement nommé "Mais c'est pas vrai, y'a une erreur ! WTF ?"</p>

<p>Sauf que, forum à l'appui, d'erreur il n'y a point.</p>

<p>Pour éviter tout système de rebond (Entendez un méchant qui a piraté un serveur et qui voudrait accéder à la console pour en acheter un autre), on bloque, sans possibilité de rémission. Sans se poser de questions. Et tant qu'à faire, sans même un message un peu verbeux (403 Forbidden Apache powaa!)</p>

<p><strong>Résultat des courses : </strong>Vous vous résignez finalement à charger votre iso via votre machine locale, avec votre connexion ADSL à l'upload plus que douteux. Si en plus vous êtes en entreprise et que tout le reste de la boîte utilise la même connexion pour powner du FTP ou télécharger des contenus sous licence Creative Commons, préparez vous à avoir des remontées violentes. Mais bon vous bossez, c'pas d'votre faute hein.</p>

<p>&nbsp;</p>

<p><strong>Résultat des courses, 2eme vue :&nbsp;</strong>Vous avez perdu 1h à installer votre machine virtuelle, pour rien. Vous perdez ensuite 5h de votre journée à regarder avancer une jauge à la con. Bah oui, parce que comme c'est sécurisé, il y a un timeout, donc si jamais vous êtes pas là à cliquer régulièrement sur votre applet java pour lui dire que sisi, ça bosse toujours, vous êtes bons pour recommencer.</p>

<p>Accessoirement, avec un upload moyen de 70Kb/s, vous allez encore moins vite qu'un lecteur CD 1x. Croyez moi, s'pas gagné.</p>

## <a name="Et en plus...">Et en plus...</a>

<p>Et en plus, comme votre hébergeur <strike>est toujours un gros extremiste</strike> sécurise son réseau, pendant que vous configuriez votre petite machine virtuelle (avant de vous rendre compte que ça servait à rien, j'entends) vous avez commencé par la lancer avec une IP privée, comme ça à l'arrache.</p>

<p>Sauf que, ni une, ni deux, elle a envoyé quelques paquets innocent avec son adresse privée et sa MAC bidon sur le réseau de votre hébergeur (bah oui, pas eu le temps de configurer un NAT, ou même d'y penser). Celui-ci, malin, va bloquer ces paquets rapidement.</p>

<p>Mais aussi, parce que ça ne mange pas de pain, il va également bloquer tout accès à votre serveur ! Plus de SSH, plus d'interface web, même plus de console en ligne ! (Sans parler d'ILO ou de DRAC)</p>

<p>Pendant un temps plus ou moins variable, vous ne pouvez plus rien faire. Sans même savoir pourquoi d'ailleurs, puisque vous avez juste droit à un gros message rouge vous disant que "c'est con hein, mais c'est bloqué. C'est votre faute z'aviez qu'à pas &lt;Insert blank here&gt;".</p>

<p>Pour peu que à peine le réseau revenu vous refassiez la même erreur 2, 3, 4 fois avant de comprendre ?</p>

<p>Pour peu que vous ayiez lancé un ping sur votre VM pour tester la connexion, et qu'au fur et à mesure que le réseau soit débloqué, la VM continue son ping et vous rebloque de nouveau...</p>

<p>Vous avez perdu encore une petite heure, au bas mot. (Soyons large, on est des pros ici, on comprends vite d'où ça peut venir ces petites erreurs là.)</p>

<p>&nbsp;</p>

## Mais au final

<p>Au final, vous avez déjà perdu 7h, vous êtes plus à ça près, s'pas ?</p>

<p>Et puis, ça vous laisse plein de temps pour chercher avec votre connexion surchargée le nom d'un autre hébergeur chez qui migrer au plus vite... Pour vous rendre compte qu'en fait, ils sont tous hors de prix tout en étant pire dans leur gestion réseau.</p>

<p>&nbsp;</p>

<p><strong>Conclusion :&nbsp;</strong>L'auto-hébergement, c'est le bien !</p>

<p>[MODE GROGNE : OFF]</p>

<p><em>Nota : bien sûr, personne ne doit se sentir insulté ou calomnié par ce texte, résultat de mon cerveau brouillon et sans prétention devant tant de capacités techniques employées pour faire le bien.</em></p>
