---
Title: Installer Openvz 7 à distance
subtitle: Bonus : chiffrez le intégralement
date: '2017-11-23 23:31'
Authors: Victor Héry
Slug: installer-openvz7-a-distance
Tags: 'Openvz7, Installation distante, Chiffrement, LUKS'
keywords: 'Openvz7, Installation distante, Chiffrement, LUKS'
Status: published
---

[TOC]

# Vous n'avez accès à rien ?
Pas d'ipmi, pas de KVM, pas de console ? Et pourtant vous voulez installer votre serveur openvz ?

C'est possible et dans cet article, on va voir comment vous pouvez vous en tirer !  
En bonus, parce que sinon ça ne serait pas marrant, on va également voir comment chiffrer le serveur et lancer un serveur SSH au boot pour entrer le mot de passe et déchiffrer le disque.

Vous aurez ainsi un serveur openvz entièrement chiffré pour faire tourner vos container ou vos VM, ce qui est très appréciable à notre époque où la confidentialité des données est un problème important.  
Vous pouvez faire confiance à votre hébergeur (ou à ceux qui pourraient lui demander d'accéder à vos données), mais personnellement je pense que si vous ne vous mettez pas dans la situation ou vous **devez** lui faire confiance, c'est encore plus simple ;-)

Je ne vais par contre pas vous mentir, sans accès visuel, le debug en cas de problèmes peut être trèèèèès relou.  
Je vais ici décrire la procédure complète qui a fonctionné plusieurs fois chez moi, mais il a fallut de nombreux tests... Si jamais vous ne vous en sortez pas, n'hésitez pas à commenter, j'essaierai de vous aider au mieux !

**Attention !** Cela va de soit, mais cette procédure va totalement effacer les données de votre serveur. S'il n'est pas neuf et que vous souhaitez récupérer des données dessus, pensez à les backuper ;-)

# Prérequis
Je le dis tout de suite, il va vous falloir quand même quelques trucs pour vous faciliter la vie et permettre l'installation.

 * Un serveur HTTP (pas hébergé sur le serveur que vous installez !) accessible par le serveur que vous souhaitez installer. Voir ci-dessous.
 * Un client VNC. Je vous conseille [Reminna](http://www.remmina.org/wp/) si vous utilisez Linux. Sous Windows, je ne peux pas vous aider.
 * Un client SSH. Openssh sous Linux, ou [putty](http://www.putty.org/) sous Windows.
 * Il vous faut un mode rescue avec votre hébergeur... Même minimaliste, avec au moins un accès SSH, nécessaire pour le chroot.
 * Installer votre serveur sur votre OS préféré pour le préparer, j'utiliserai pour ma part Debian 9.

## Préparer le serveur HTTP
Pour permettre l'installation à distance, il va falloir faire en sorte que les fichiers de l'image ISO openvz7 soient accessibles en HTTP pour que votre serveur puisse récupérer les fichiers à distance lors de son démarrage.  
**Ces manipulations doivent donc avoir lieu sur un AUTRE serveur que celui que vous allez installer !**  
Je partirai également du principe que vous avez un serveur HTTP. N'importe quel serveur web fera l'affaire, apache, nginx, IIS, c'est selon vos préférenes ou ce qui est disponible.

Téléchargez votre iso depuis les [mirroirs openvz](https://download.openvz.org/virtuozzo/releases/7.0/x86_64/iso/). Vous pouvez prendre l'image classique (pas la netinstall).  
Il va vous falloir mettre à disposition les fichiers de l'image via HTTP. On va donc monter l'ISO sur votre serveur web, puis poser les fichiers dans un vhost pour qu'ils soient disponibles.

    mount openvz-iso-7.0.5-netinstall.iso /votre_repertoire_web/

Faites en sortes que ce répertoire soit accessible sur une adresse type http://votredomain.tld/vz. Il doit pointer directement dans le répertoire où est montée l'image.  
Notez cette adresse dans un coin, ça va resservir. :-)

## Préparer le serveur pour l'installation
### Nettoyez les disques en mode rescue
Redémarrer votre serveur sur votre mode rescue, pour nettoyer à fond les disques dur de votre serveur avant l'installation.  
Pour ça, on va utiliser l'outil [Shred](https://fr.wikipedia.org/wiki/Shred_(commande_unix)). En mode rescue, vous pouvez effacer vos disques tranquillement.  
Je vous conseille d'utliser au moins 2 passes, tout en remplissant de zéro pour être carré :  

    shred -n 2 -z /dev/sda

Ca peut prendre pas mal de temps selon la taille des disques, jusqu'à plusieurs heures... Lancez le de nuit si possible, mais pour que le chiffrement soit efficace, mieux vaut que le disque ne soit pas prédictible :-)

### Poser les fichiers d'installation
Pour télécharger les fichiers nécessaires au lancement de l'installation, il vous faudra un OS installé sur votre serveur.  
Vous aurez besoin de générer un grub et de poser quelques fichiers dans /boot donc un Linux selon votre préférence. Pour ma part, je part sur un debian.

Réinstallez donc votre serveur via le système d'installation de votre hébergeur, connectez vous en SSH, et go !

Récupérez via votre serveur HTTP (celui avec l'ISO montée) les 2 fichiers d'install PXE :

    wget http://votredomaine.tld/vz/images/pxeboot/initrd.img -O /boot/initrd_ovz7.img
    wget http://votredomaine.tld/vz/images/pxeboot/vmlinuz -O /boot/vmlinuz_ovz7

Notez la configuration réseau de votre serveur. Il vous faut adresse IP, passerelle, masque réseau et serveurs DNS :
    ip address show
    cat /etc/resolv.conf

Enfin, récupérez l'id de votre disque dur pour le menu de boot :-)
    blkid /dev/sda1 # Récupérez l'id de la partition de votre /boot

On va ensuite éditer grub pour ajouter une entrée spécifique pour booter sur l'installeur :

    vim /etc/grub.d/40_custom

Et ajoutez après les commentaires le contenu suivant :  
**Attention à ne pas enlever le ``exec tail -n +3 $0`` dans le fichier**

    menuentry 'NetInstall' {
    	load_video
    	set gfxpayload=keep
    	insmod gzio
    	insmod part_msdos
    	insmod ext2
    	set root='hd0,msdos1'
    	if [ x$feature_platform_search_hint = xy ]; then
    	  search --no-floppy --fs-uuid --set=root --hint='hd0,msdos1' ID_DISQUE
    	else
    	  search --no-floppy --fs-uuid --set=root  ID_DISQUE
    	fi
    	linux /boot/vmlinuz_ovz7 vnc vncpassword=guigui inst.vncpassword=guigui headless inst.repo=http://votredomaine.tld/vz/ ip=IP_SERVEUR netmask=MASQUE_SERVEUR gateway=PASSERELLE_SERVEUR dns=DNS_SERVEUR nameserver=DNS_SERVEUR biosdevname=0 net.ifnames=0 ksdevice= bootproto=static lang=en_US keymap=us text noipv6
    	initrd /boot/initrd_ovz7.img
    }

Cette configuration va permettre de redémarrer le serveur sur une console VNC pour permettre d'installer openvz7 en mode graphique à distance :-)  
Editez bien entendu les valeurs ci dessous :

  * ID_DISQUE : l'id de la partition récupérée plus tôt
  * IP_SERVEUR : l'adresse IP du serveur
  * MASQUE_SERVEUR : le masque de sous réseau de votre serveur
  * PASSERELLE_SERVEUR : la passerelle par défaut de votre serveur
  * DNS_SERVEUR (x2) : l'IP du serveur DNS remontée plus tôt
  * mot de passe vnc : il doit faire entre 4 et 6 caractères, donc si vous le changez, restez dans ces critères

Puis regénérez la configuration de grub :

    grub-mkconfig -o /boot/grub/grub.cfg

Et enfin, configurez votre serveur pour redémarrer sur votre installeur au prochain reboot. Cette option permettra de redémarrer sur l'entrée grub, mais une fois seulement.  
Ainsi, si jamais il ne démarre pas ou que l'installation se passe mal (avant le formatage bien sûr), un simple redémarrage électrique le fera redémarrer sur son OS de base.

    grub-reboot NetInstall

On va maintenant pouvoir redémarrer et récupérez la console VNC.

# Installation d'openvz 7 !
Redémarrez votre serveur, et lancez un ping sur son adresse IP. Dès qu'il répond, lancez votre console VNC sur l'IP du serveur, avec le port 5900.  
**Attention**, malgré pas mal de recherches et de tests, je n'ai pas réussi à monter une connexion chiffrée pour ce VNC... Sachez donc que tout ce que vous y entrerez sera potentiellement récupérable en clair sur Internet !  
Préparez vous donc à utiliser des mots de passe temporaires pour le chiffrement des disques et les accès utilisateurs. Nous les changerons dès l'installation terminé via une connexion SSH un peu plus robuste :-)

Vous avez accès à la console VNC en mode graphique, donc rien de très compliqué pour l'installation. La [documentation officielle](https://docs.openvz.org/openvz_installation_guide.webhelp/_installing_openvz_in_graphics_mode.html) est parfaite à ce niveau là.

Normalement, la configuration réseau est déjà faite gace aux options de grub, doncrien à toucher.

Le partitionnement des disques par défaut est en théorie suffisant, **mais** c'est le moment de penser à cocher "Encrypt" !  
Vous devez cocher la case pour toutes les partitions sauf /boot et la petite partition "BIOS Boot".

En sortant du menu de partionnement, l'installeur vous demandera les mots de passe pour le chiffrement. Il est important de mettre le même mot de passe pour toutes les partitions.  
Attention là encore à ne pas mettre le mot de passe final, car la connexion à VNC n'est pas chiffrée !

Laissez ensuite l'installation se terminer tranquillement, puis passez à la suite.

# Serveur SSH et réseau au boot
Pour cette partie, il va falloir redémarrer votre serveur en mode rescue de nouveau.  
En effet, votre serveur est désormais installé, ses disques sont chiffrés, mais vous n'avez pour l'instant aucun moyen de taper le mot de passe demandé au boot pour déchiffrer les disques !  
On va donc modifier directement le initramfs pour y intégrer un petit serveur SSH qui se lancera au boot, et qui permettra ainsi de taper le mot de passe au démarrage en toute sécurité.

## SSH dans le initramfs
Une fois revenu en mode rescue, préparez vous à monter votre système en chroot. Je part ici du principe que vous avez gardé le partionnement par défaut, donc pensez à adapter le nom des partitions dans la suite si besoin.  
On va commencer par changer les mots de passe bidon des partitions.

    vgchange -a y
    cryptsetup luksChangeKey /dev/sda3 # On change le mot de passe du swap
    cryptsetup luksChangeKey /dev/mapper/virtuozzo-root # On change le mot de passe du /
    cryptsetup luksChangeKey /dev/mapper/virtuozzo-vz # On change le mot de passe du vz
    cryptsetup luksOpen /dev/mapper/virtuozzo-root crypt_root
    mkdir -p /mount/openvz
    mount /dev/mapper/crypt_root /mount/openvz
    mount /dev/sda2 /mount/openvz/boot
    mount -o bind /dev/ /mount/openvz/dev/
    mount -o bind /sys/ /mount/openvz/sys
    mount -o bind /proc/ /mount/openvz/proc/
    chroot /mount/openvz

Une fois dans le chroot, les choses marrantes reprennent ;-)

La connexion au boot se fera exclusivement avec une clef SSH. Si vous avez besoin de générer une clef SSH, vous pouvez vous référer à [cet article](https://blog.victor-hery.com/2012/08/connexion-ssh-par-cle.html#generation-de-la-cle-les-choses-serieuses-commencent) .  
Commencez donc par poser votre clef ssh dans le chroot directement.

    mkdir -p /root/.ssh
    vim /root/.ssh/authorized_keys # Entrez votre clef SSH personnelle
    chmod 700 /root/.ssh
    chmod 600 /root/.ssh/authorized_keys

On va ensuite utiliser le splendide plugin [dracut-crypt-ssh](https://github.com/dracut-crypt-ssh/dracut-crypt-ssh) pour modifier le initramfs via dracut.  

    yum install epel-release
    wget -O /etc/yum.repos.d/rbu-dracut-crypt-ssh-epel-7.repo https://copr.fedorainfracloud.org/coprs/rbu/dracut-crypt-ssh/repo/epel-7/rbu-dracut-crypt-ssh-epel-7.repo
    yum install dracut-crypt-ssh
    dracut --force

La commande dracut ne doit rien vous renvoyer mais peut être longue à exécuter... Si vous avez une erreur, vous devrez chercher la solution avant le prochain redémarrage, car votre initramfs risque d'être HS. Au pire, le mode rescue vous serez extrêmement utile !

Une fois que le initramfs a été généré sans erreur, on va s'occuper du réseau.

## Réseau via initramfs

Reprenez ici la configuration réseau que vous aviez noté plus haut pour le boot VNC, ça va resservir.

On va éditer le fichier `/etc/default/grub` pour demander à grub de monter le réseau au démarrage.

     GRUB_CMDLINE_LINUX="... rd.neednet=1 ip=IP_SERVEUR::PASSERELLE_SERVEUR:MASQUE_SERVEUR:centos:enp0s3:off dns=DNS_SERVEUR nameserver=DNS_SERVEUR"

Ici on va réutiliser les paramètres réseaux, mais ce qu'il faut surtout noter, c'est la partie "enp0s3".  
Il s'agit de votre interface réseau, nommée d'après les paramètres du Bios de votre serveur, les classiques ethX ayant été abandonnés depuis les derniers noyaux. Si jamais vous ne la connaissez pas, il est possible de forcer eth0, cf la section troubleshooting plus bas :-)

Une fois le fichier enregistré, regénérez la configuration grub :

    grub2-mkconfig --output /etc/grub2.cfg

Une fois la configuration regénéré sans erreurs, vous pouvez quitter le chroot, démonter les disques et préparer le redémarrage.

    exit
    umount /mount/openvz/dev
    umount /mount/openvz/sys
    umount /mount/openvz/proc
    umount /mount/openvz/boot
    umount /mount/openvz
    cryptsetup luksClose crypt_root
    vgchange -a n

N'oubliez pas de désactiver le mode rescue de votre serveur, pour éviter qu'il redémarre en mode rescue ! Puis redémarrez le serveur.

# Last reboot (?)

Si tout s'est bien passé, au bout de quelques minutes (selon votre serveur), le réseau devrait se remettre à pinguer. Quelques instant plus tard, le SSH devrait être disponible.  
**Attention**, le SSH écoute sur le port **222** dans cette configuration.

    % ssh root@IP_SERVEUR -p 222
    # console_peek # Pour voir ce qu'affiche la console, notamment la demande de mot de passe
    # console_auth # Pour vous permettre de taper la clef des disques dur

Si le mot de passe est correcte, le boot va continuer et votre shell va se terminer et votre serveur terminer de démarrer tranquillement.  
Vous devriez ensuite pouvoir vous connecter de manière tout à fait classique avec vos identifiants (le mot de passe root indiqué à l'installation).

Si jamais initramf ne continue pas, n'hésitez pas à utiliser de nouveau `console_peek` pour récupérer le contenu de la console et voir ce qui coince.

# Troubleshooting
Cette procédure est normalement fonctionnelle, mais pas mal de chose peuvent mal se passer malheureusement, sans accès console, c'est difficile à debuguer...  
Personnellement, j'ai redémarré mon serveur en mode rescue/redémarrage classique plus de 90 fois pour rédiger cet article :-D  
Je vais ici parler des problèmes que j'ai rencontré pendant mes tests.

## Pas de VNC
Si votre serveur ne démarre pas en VNC, il se peut que le réseau ne monte pas. Si vous avez suivi la consigne avec la commande `grub-reboot NetInstall`, il vous suffit de redémarrer le serveur via l'interface de votre hébergeur, et il repartira par défaut sur votre OS de préparation.

À partir de là, vérifiez votre configuration réseau et que votre grub contient le tout. Sous Debian, il est dans `/boot/grub/grub.cfg`.  
Remettez un coup de `grub-reboot NetInstall` et on redémarre :-)

## L'installeur Virtuozzo plante
J'ai rencontré plusieurs crash une fois la console VNC lancée. Ils étaient dû à des paramètres incorrects dans les paramètres de boot, comme l'id du disque dur de votre menu grub, ou le layout clavier.  
Vérifiez que tout correspond bien, et notamment si vous avez tenté un `lang` différent de "en_US" ou un `keymap` différent de "us", laissez tomber et réessayez avec les bonnes options. Apparemment, l'installeur de Virtuozzo n'aime pas les autres options au démarrage...  

Notez que vous pouvez quand même configurer un layout FR une fois l'installeur lancé via l'interface graphique.

## Forcer eth0
Pour la configuration réseau de initramfs, déterminer le nom qu'aura l'interface réseau avec la prédiction Bios n'est pas forcément évident si votre OS de préparation ne l'utilise pas.  
Pour forcer à revenir à eth0, il vous suffit d'ajouter les 2 options suivantes dans votre `GRUB_CMDLINE_LINUX`, via votre chroot en mode rescue :

    ... net.ifnames=0 biosdevname=0 ...

N'oubliez pas de régnérer le grub une fois la ligne modifiée :-)

    grub2-mkconfig --output /etc/grub2.cfg

## Rien ne boot ça marche pas !

Un conseil, si vraiment ça ne fonctionne pas et que tout parait correct, déroulez toute la procédure sur une VM locale, par exemple via virtualbox, pour disposer d'une console et voir les erreurs qui peuvent s'y afficher.

Si jamais, n'hésitez pas à me laisser un petit commentaire, si je peux aider ça sera avec plaisir !

Et bon courage ;-)

# Sources et biblio
Je remercie ces différents auteurs qui m'ont permis d'arriver à ce résultat et à écrire cet article !

 * [Stockage chiffré intégral sur serveur distant ](https://blog.imirhil.fr/2017/07/22/stockage-chiffre-serveur.html)
 * [Install Virtuozzo via VNC](https://docs.virtuozzo.com/virtuozzo_7_installation_guide/installing-virtuozzo/installing-virtuozzo-via-vnc.html)
 * [Centos6 disk encryption with remote password](http://roosbertl.blogspot.com/2012/12/centos6-disk-encryption-with-remote.html)
 * [crypt-wait for dracut](https://bitbucket.org/bmearns/dracut-crypt-wait/src)
 * [Dracut crypt-ssh](https://github.com/dracut-crypt-ssh/dracut-crypt-ssh)
 * [Dracut documentation](https://www.kernel.org/pub/linux/utils/boot/dracut/dracut.html)
