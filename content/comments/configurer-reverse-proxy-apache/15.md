email: olivier@olivier-raulin.fr
date: 2013-08-28T19:29+01:00
author: Olivier
website: http://www.olivier-raulin.fr
replyto: 13md

Comme promis, j'ai donc fait la même configuration pour nginx, pour ceux que ça peut intéresser : http://blog.olivier-raulin.fr/2013/08/28/mise-en-place-dun-reverse-proxy-nginx/

J'imagine que tu as essayé dans le ProxyPassReverse de mettre https://ip/, ça ne fonctionnait pas ?
