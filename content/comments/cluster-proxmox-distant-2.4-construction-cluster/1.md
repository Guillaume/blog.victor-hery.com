email: courriel@victor-hery.com
date: 2013-02-14T14:51+01:00
author: Victor
website: http://blog.xn--hry-bma.com/
replyto: 0md

Hello,  
Je connais moins IPSec qu'OpenVPN, mais j'avais fait des tests avec il y a quelques temps. Le principal problème que je rencontrais est qu'IPSec est difficile à interfacer avec un bridge. Or, Proxmox fonctionne principalement sur ce type de système. En plus à l'époque c'était Proxmox 1.X, dont le système de cluster était de type maitre/esclave, moyennement adapté à IPSec.  
Sur Proxmox V2 et dans l'optique de ce cluster, je vois plusieurs soucis à utiliser IPSec :   
 - Il faudra faire passer du Multicast dans le tunnel. Pas sûr de voir comment faire avec IPSec, il faudrait chercher la viabilité et la possibilité de router le multicast à travers le tunnel  
 - En montant un vrai "triangle" IPSec, il y aurait un risque de spanning tree je pense (je l'ai rencontré en faisant un triangle OpenVPN au début). IPSec peut-il gérer ça ?  
 - Pour IPSec, il faut déclarer chaque plage d'IP qui est derrière le tunnel, donc impossible de mettre les même plages de chaque côtés, ce qui risque de compliquer le basculement des machines en HA, à moins de prévoir un canal de communication exprès pour la gestion du cluster (ce qui n'est pas déconnant je l'accorde :) )  
 - Dans mon cas particulier où l'arbitre est juste une machine virtuelle dans un coin, il y a le problème du NAT, jamais très apprécié par IPSec. Ce problème ne se pose pas si les 3 machines sont de vrais serveurs avec IP publiques  
 - Globalement, je n'aime pas trop IPSec, que je trouve plus complexe et moins robuste qu'OpenVPN (trop de contraintes...) (Pas de troll ! :D)  
 - Le système avec OpenVPN fonctionne plutôt bien, en étant simple à mettre en place ;)  

Cependant si tu as des arguments, j'aimerai les entendre, je suis amené à utiliser IPSec de temps en temps, et si je peux améliorer mes connaissances dessus je suis preneur ! (De même que pour améliorer ces tutos ;) )

A plush'
