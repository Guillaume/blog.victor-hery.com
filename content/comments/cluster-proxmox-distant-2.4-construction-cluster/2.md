email: ebourg@apache.org
date: 2013-02-17T01:40+01:00
author: Manu
website:
replyto: 0md

Je vois deux avantages à IPsec, la symétrie de la relation entre les hosts et un overhead sans doute plus faible qu'avec OpenVPN. J'ai aussi la vague intuition que la configuration pourrait être plus simple à l'arrivée, mais pour le moment je n'ai pas encore trouvé la bonne solution :) L'idée serait de monter un tunnel GRE laissant transiter le multicast et de le sécuriser avec IPsec en mode transport. Le tunnel GRE est vraiment très simple à mettre en place, mais je n'ai pas encore bien intégré comment le multicast y passe.

Sinon je crois qu'il y a moyen de faire sans multicast, ce qui simplifierait pas mal les débats :  
http://pve.proxmox.com/wiki/Multicast_notes#Use_unicast_instead_of_multicast
