email:
date: 2013-02-01T14:06+01:00
author: Gégé
website:

Bonjour,

j'ai trouvé votre article très intéressant et assez précis pour répondre à pas mal de questions sur le cluster HA de Proxmox.  
Je suis moi-même en train de monter un cluster à trois noeuds avec HA et je retrouve dans votre article quelques réponses à mon questionnement.  
J'en ai pourtant encore quelques unes (des questions) qui n'ont pas encore trouvé de réponse.  
exemple: dans un cluster ha proxmox quelle doit être l'attitude des noeuds valides lorsqu'un troisième noeuds à perdu le contact (perte alimentation par exemple) . Dan mon cas lors de la perte totale d'un noeud il ne se passe rien jusqu'au retour du noeud arrêté . Les VM configurées avec HA et présentes sur ce noeud migrent à ce moment là. Je ne pense pas que ce soit un fonctionnement normal.  
J' ajoute que mes test de fencing (notamment avec l'outil fence_node ou en debranchant le reseau d'un des noeuds)  ont donnés des résultats satisfaisant.  
si vous avez un petit moment de libre à consacrer a ce problème,  je vous en serais très reconnaissant .  
merci d'avance  
cordialement
