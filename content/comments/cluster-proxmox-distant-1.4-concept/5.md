email: courriel@victor-hery.com
date: 2013-02-02T09:16+01:00
author: Victor
website: http://blog.xn--hry-bma.com/
replyto: 4md

Bonjour Gégé,  
Effectivement, ce n'est pas un comportement normal. Normalement, dès que le 3eme serveur disparait, toutes les machines en HA devraient migrer quasi-instantanément vers les serveurs de secours.  
Pour trouver ce qui cloche, il faudrait regarder ce qui se passe dans les logs. Normalement, le syslog (/var/log/syslog) donne pas mal d'informations sur le fonctionnement du cluster. Dès qu'un serveur disparait, ça apparait dans les logs sous la forme d'une dizaine de lignes expliquant la nouvelle configuration (qui est parti, qui est resté, état du quorum, etc). De plus, un des serveur restant est élu aléatoirement pour s'occuper du fencing. Le résultat du fencing est également écrit dans les logs.  
Il faut savoir que le cluster est freezé tant que le fencing n'est pas réussit, c'est à dire qu'aucune machine en HA ne migrera si le fencing échoue. Dans le fonctionnement, le serveur élu pour effectuer le fencing va essayer toutes les méthodes qui ont été définies dans le cluster.conf pour la machine en panne. Si elles échouent toutes, il va les réessayer, et ainsi de suite jusqu'à ce qu'une des méthodes réussissent.  
Il est possible que lorsque tu déconnectes ton 3eme serveur, le système qui te sert de fencing se déconnecte aussi ? Auquel cas, il est possible que le fencing n'arrive jamais à se faire et du coup les machines en HA ne migrent pas. Et dès que le serveur revient, le fencing aussi, donc il réussit et les machines migrent.  
Il y a également une sécurité lorsque leHA est activé, qui fait qu'une machine qui est sorti du quorum et qui revient va automatiquement être exclue des décisions tant qu'elle n'aura pas été redémarré proprement. (En fait, le cluster "tue" le gestionnaire clvm du serveur fautif, cela apparait dans les log sous la forme "clvm killed by node X")  
Il est aussi possible de définir des priorité, des temps de migration, etc pour chaque machine dans le cluster.conf, peut être as-tu un problème à ce niveau là ?  
Pour en savoir plus, tu peux essayer de regarder l'état du cluster lorsqu'un de tes serveurs est tombé, à l'aide des commandes suivantes :  

- pve n : va te montrer l'état des nodes (Online, offline, est ce que rgmanager tourne)  
- pve s : va te montrer l'état du cluster (notamment est ce qu'il a la quorum ou non)  
- clustat : va te donner des informations plus précises sur le HA, notamment l'état des VM déclarées en HA, et le dernier serveur où elles ont été vues  

Voila, c'est tout ce que je peux dire avec ces éléments.  
Bon courage !  

A plush'

Victor
