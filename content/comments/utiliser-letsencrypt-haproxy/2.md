email: courriel+blog@victor-hery.com
date: 2016-03-02T21:56+01:00
author: Victor
website: https://blog.victor-hery.com/
replyto: 1md

Hello Pierre Philippe,

Le concept de Let's Encrypt est de créer un certificat tout ce qu'il y a de plus classique. Ici, l'astuce est que normalement, tes DNS pointent sur ton haproxy, puisque c'est lui qui route ensuite via les  ACL vers les backend.  
C'est donc lui qui doit posséder le certificat HTTPS pour le servir aux visiteurs. En l'occurence, haproxy va déchiffrer le flux avec le certificat, puis envoyer le flux non chiffré vers les backend. (Il est possible de rechiffrer le flux avant de l'envoyer, mais ce n'est pas le sujet)

Donc normalement, tu dois utiliser comme nom de domaine celui pour lequel tu veux un certificat, et lancer let's encrypt depuis le serveur en question. Il faut également (si tu as plusieurs adresses IP sur le haproxy), que haproxy écoute sur l'adresse où Let's encrypt va envoyer la requête de vérification.  

Voila pour le concept :)

Ensuite pour ton problème plus précisément, à première vue ta configuration semble correct. Le invalid key semble montrer que let's encrypt n'est pas content de la clef généré, mais pourtant c'est lui même qui la génère.  
Est ce que tu as modifié la configuration de Let's Encrypt pour générer une clef plus petite que 1024bits ?  
Egalement, quelle version d'openSSL utilise-tu ? Il est possible avec les différentes failles de sécurité sorties ces derniers temps que Let's Encrypt ait certain critères à ce propos même si je ne l'ai pas remarqué.  

N'hésite pas à me préciser plus comment tu utilise Let's Encrypt, je pourrais peut être t'en dire plus :) (sur quel OS, la ligne exacte que tu lances, en cachant le domaien bien sur, la version d'openssl, etc)

Bon courage !
