email: courriel@victor-hery.com
date: 2013-02-10T12:48+01:00
author: Victor
website: http://blog.xn--hry-bma.com/
replyto: 6md

Hello,

Pour ma part, j'aime bien l'auto-hébergement pour son principe. Il est vrai que la bade passante est un vrai problème, et du coup ça reste de l'hébergement non-industriel. Le jour où ce blog attirera vraiment les foules, je repenserai sans doute à mon schéma d'utilisation, mais je n'y suis pas encore ! Et un peu de QOS peut régler bien des choses :)

Niveau coûts de la machine et electricité, c'est plus difficile à calculer, car si en effet un hébergement bas de gamme chez OVH ne coute pas grand chose, ça reste un paiement au mois. Acheter un serveur, c'est un cout fixe. Pour ma part, mon serveur perso m'a couté moins de 200€, il est très largement évolutif, et il consomme très peu.
Il ne fait également aucun bruit :)
Je t'invite à jeter un coup d'oeil aux articles du développeur neurasthénique sur ce sujet, il sont plutôt intéressants :
http://blog.developpeur-neurasthenique.fr/auto-hebergement-composants-pour-une-nobox-libre-et-acentree.html
http://blog.developpeur-neurasthenique.fr/auto-hebergement-un-an-apres-petit-bilan-avant-le-debut-des-hostilites.html

Pour ma part, j'avoue que je n'ai pas encore eu le courage de faire un article sur ma nobox perso. Je suis plutôt parti sur une solution sous forme de tour, un peu plus encombrante mais plus évolutive. Ca permet également d'avoir du matériel un peu plus puissant avec des ventilateur faisant moins de bruit. Et l'énorme avantage de Proxmox c'est qu'avec un bon backup, il est facile de basculer ailleurs si jamais un problème se pose :)
(Bon, il faut avoir prévu le coup...)
Typiquement, mes tutos sur un cluster Proxmox ne se pose pas vraiment dans une problématique d'auto-hébergement, mais plutôt dans une idée de services à haute disponibilité ! ;)
Tout est une question d'énergie et de temps à y passer.
L'auto-hébergement permet quand même de bien s'amuser, sur des points qui serait impossible en hébergé (notamment la QOS)
