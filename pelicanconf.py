#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Victor'
SITENAME = u'Blog de Victor Hery'
SITEURL = ''
DEFAULT_CATEGORY = 'Système'
USE_FOLDER_AS_CATEGORY = False
THEME='./theme'
DIRECT_TEMPLATES = (('index', 'tags', 'categories','archives', 'search', '404'))

LANDING_PAGE_ABOUT ={'title' : 'Je blog techniques, jardins et coups de gueule'}


PATH = 'content'
STATIC_PATHS = ['theme/images', 'images']
ARTICLE_SAVE_AS = '{date:%Y}/{date:%m}/{slug}.html'
ARTICLE_URL = '{date:%Y}/{date:%m}/{slug}.html'
SUMMARY_MAX_LENGTH = 50
DEFAULT_PAGINATION = 4

### Plugins ###
PLUGIN_PATHS = ['../pelican-plugins']
PLUGINS = ['pelican_comment_system', 'extract_toc', 'sitemap']

#sitemap
SITEMAP = {
    'format': 'xml',
    'priorities': {
        'articles': 0.5,
        'indexes': 0.5,
        'pages': 0.5
    },
    'changefreqs': {
        'articles': 'monthly',
        'indexes': 'daily',
        'pages': 'monthly'
    }
}

# Comments
PELICAN_COMMENT_SYSTEM = True
PELICAN_COMMENT_SYSTEM_IDENTICON_DATA = ('author',)
PELICAN_COMMENT_SYSTEM_DIR = 'comments'
PELICAN_COMMENT_SYSTEM_AUTHORS = {
    ('Victor',): "images/authors/face.png",
    ('victor',): "images/authors/face.png",
    ('victorhery',): "images/authors/face.png",
}
COMMENT_URL = "#comment_{slug}"
COMMENTS_INTRO = "Si vous avez des questions, si quelque chose n'est pas clair, n'hésitez pas à commenter !"

#Markdown PLUGINS
MARKDOWN = {
    'extension_configs': {
        'markdown.extensions.codehilite': {'css_class': 'highlight'},
        'markdown.extensions.extra': {},
        'markdown.extensions.meta': {},
        'markdown.extensions.toc': {},
    },
    'output_format': 'html5',
}

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'fr'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('Blog statique avec Pelican', 'http://getpelican.com/'),
         ('Un blog où l\'on parle d\'openvz, de QOS et d\'auto-hébergement', 'http://blog.developpeur-neurasthenique.fr/'),
         ('Projets OpenVZ-diff-backups', 'http://projets.developpeur-neurasthenique.fr/projects/openvz-diff-backups/'),
         ('Thème graphique Elegant', 'http://oncrashreboot.com/elegant-best-pelican-theme-features'),)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)
SOCIAL = ''

DEFAULT_METADATA = {
    'status': 'draft',
    'lang': 'fr',
}

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
